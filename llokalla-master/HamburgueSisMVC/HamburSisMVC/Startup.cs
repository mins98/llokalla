﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HamburSisMVC.Startup))]
namespace HamburSisMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
