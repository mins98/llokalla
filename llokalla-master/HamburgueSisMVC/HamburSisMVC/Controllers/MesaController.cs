﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburSisMVC.Models;
using Common;
using BRL;


namespace HamburSisMVC.Controllers
{
    public class MesaController : Controller
    {
       // GET: Usuario
        public ActionResult ListMesa()
        {
			if (Session["ObjectSession"] != null)
			{
				//llama a la vita de la lista mandandole la lista recuperada de un modelo
				MesaListModel _list = MesaListModel.GetMesaList();
				return View(_list);
			}
			else
				return RedirectToAction("LogIn", "LogIn");
		}
        // GET: Usuario/Create
        public ActionResult NewMesa()
        {
            MesaModel usm = new MesaModel();
            return View(usm);//abre la vista de nuevo mandando un modelo vacio eso es obligatorio, ta que luego lo recupera pero lleno
        }
        // POST: Usuario/Create
        [HttpPost]
        public ActionResult NewMesa(MesaModel usm)//el metodo post de nuevo, los post se deben llamar igual, varia sus parametros, en caso de este pide un modelo
        {
            try
            {
                // TODO: Add insert logic here
                Mesa mes = new Mesa(usm.nroMesa, usm.ubicacion, usm.cantidadMaxima);
                MesaBRL.Insert(mes);

                return RedirectToAction("ListMesa");//devuelve a la lista
            }
            catch
            {
                return View();
            }
        }
        // GET: Usuario/Edit/5
        public ActionResult EditMesa(byte id)// pide un id para recuperar el dato
        {
           
            MesaModel us = MesaModel.recuperar(id);//modelo que recupera el usuario
            return View(us);//llama a su vista cargando el resultado
        }
        // POST: Usuario/Edit/5
        [HttpPost]
        public ActionResult EditMesa(byte id, MesaModel usm)
        {
            //similar al post de insert
            try
            {
                Mesa mes = new Mesa(id, usm.nroMesa, usm.ubicacion, usm.cantidadMaxima, usm.activo);
                MesaBRL.Update(mes);

                return RedirectToAction("ListMesa");
            }
            catch
            {

                return View();
            }
        }
        // GET: Usuario/Delete/5
        public ActionResult Delete(byte id) //contrlador que pide un id, no tiene vista propia
        {
            try
            {
                MesaBRL.Delete(id);//borra un usuario
                return RedirectToAction("ListMesa");
            }
            catch
            {
                return RedirectToAction("ListMesa");
            }
        }
    }
}