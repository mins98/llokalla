﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburSisMVC.Models;

namespace HamburSisMVC.Controllers
{
    public class CategoriaController : Controller
    {
        private static List<CategoriaModel> _CategoriaModel;

        public static List<CategoriaModel> CategoriaModel
        {
            get
            {

                _CategoriaModel = new List<CategoriaModel>();
               
                    _CategoriaModel.Add(new CategoriaModel
                    {
                        idCat = 0,
                        nombre ="Admin"
                    });
                _CategoriaModel.Add(new CategoriaModel
                {
                    idCat = 1,
                    nombre = "Empleado"
                });
                _CategoriaModel.Add(new CategoriaModel
                {
                    idCat = 2,
                    nombre = "Cliente"
                });

                return _CategoriaModel;
            }
            set { _CategoriaModel = value; }
        }
        // GET: Categoria
        public ActionResult Index()
        {
            return View();
        }
    }
}