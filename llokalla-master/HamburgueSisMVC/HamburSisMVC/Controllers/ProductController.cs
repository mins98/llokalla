﻿using BRL;
using Common;
using HamburSisMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HamburSisMVC.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult ListaProductos()
        {
			//llama a la vita de la lista mandandole la lista recuperada de un modelo
			if (Session["ObjectSession"] != null)
			{
				ProductoListModel _list = ProductoListModel.GetProductosList();
				return View(_list);
			}
			else
				return RedirectToAction("LogIn", "LogIn");
		}
		public ActionResult NewProduct()
		{
            ProductoModel usm = new ProductoModel();
			return View(usm);//abre la vista de nuevo mandando un modelo vacio eso es obligatorio, ta que luego lo recupera pero lleno
		}
		// POST: Usuario/Create
		[HttpPost]
		public ActionResult NewProduct(ProductoModel productomodel)//el metodo post de nuevo, los post se deben llamar igual, varia sus parametros, en caso de este pide un modelo
		{
			try
			{
				// TODO: Add insert logic here
				Producto producto = new Producto(productomodel.nombreProducto, productomodel.precio, productomodel.tipo, productomodel.descripcion);
				ProductoBRL.Insert(producto);

				return RedirectToAction("ListaProductos");//devuelve a la lista
			}
			catch
			{
                return View();
			}
		}
		// GET: Usuario/Edit/5
		public ActionResult EditProduct(byte id)// pide un id para recuperar el dato
		{

			ProductoModel productoModel = ProductoModel.recuperar(id);//modelo que recupera el usuario
			return View(productoModel);//llama a su vista cargando el resultado
		}
		// POST: Usuario/Edit/5
		[HttpPost]
		public ActionResult EditProduct(byte id, ProductoModel productModel)
		{
			//similar al post de insert
			try
			{
				Producto producto = new Producto(productModel.idProducto, productModel.nombreProducto, productModel.precio, productModel.tipo, productModel.activo, productModel.descripcion);
				ProductoBRL.Update(producto);

				return RedirectToAction("ListaProductos");
			}
			catch
			{

                return View();
            }
		}
	
		// GET: Usuario/Delete/5
		public ActionResult Delete(byte id) //contrlador que pide un id, no tiene vista propia
		{
			try
			{
				ProductoBRL.Delete(id);//borra un usuario
				return RedirectToAction("ListaProductos");
			}
			catch
			{
				return RedirectToAction("ListaProductos");
			}
		}
	}
}