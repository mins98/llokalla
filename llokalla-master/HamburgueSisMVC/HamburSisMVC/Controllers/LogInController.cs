﻿using Common;
using HamburSisMVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BRL;
using System.Web.Mvc;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace HamburSisMVC.Controllers
{
    public class LogInController : Controller
    {
        // GET: LogIn
        public ActionResult Registrar()
        {
			CargarCategorias();
			CargarGeneros();
			UsuarioModel uesrModel = new Models.UsuarioModel();

			return View(uesrModel);
        }
		[HttpPost]
		public ActionResult Registrar(UsuarioModel userModel)//el metodo post de nuevo, los post se deben llamar igual, varia sus parametros, en caso de este pide un modelo
		{
			try
			{
				if (userModel.password == userModel.repeatPassword && UsuarioBRL.GetUser(userModel.userName))
				{
					// TODO: Add insert logic here
					Usuario user = new Usuario(userModel.userName, userModel.password, userModel.repeatPassword, 1);//crea un nuevo usuario con los resultados del modelo
					Persona person = new Persona(userModel.persona.nombre, userModel.persona.primerAp, userModel.persona.segundoAp, userModel.persona.fechaNac, userModel.persona.direccion, userModel.persona.ci, userModel.persona.genero);
					List<Telefono> tel = new List<Telefono>();
					tel.Add(new Telefono(userModel.tel.numero, 1));
					UsuarioBRL.Insert(person, user, tel);//usa el brl

					return RedirectToAction("LogIn");
				}
				else
				{
					return RedirectToAction("Registrar");
				}
			}
			catch(Exception ex)
			{
				return View();
			}
		}
		public ActionResult LogIn()
		{
			UsuarioModel uesrModel = new Models.UsuarioModel();

			return View(uesrModel);
		}
		[HttpPost]
		public ActionResult LogIn(UsuarioModel userModel)
		{
			try
			{
				var response = Request["g-recaptcha-response"];
				//string secretKey = "6LdpL30UAAAAAOhbkL2oX_B5k5NhckpghLBX9JsA";
				//var client = new WebClient();
				//var result = client.DownloadString(string.Format("https://www.google.com/recaptcha/api/siteverify?secret={0}&response={1}", secretKey, response)); var obj = JObject.Parse(result);
				//var status = (bool)obj.SelectToken("success");

				if ( UsuarioBRL.Login(userModel.userName, userModel.password).Rows.Count > 0)
				{//ID
					Session["ObjectSession"] = UsuarioBRL.getUsuario(UsuarioBRL.SelectUserID(userModel.userName, userModel.password));
					return RedirectToAction("index", "home");
				}
				else
				{
					return RedirectToAction("LogIn");
				}
			}
			catch (Exception ex)
			{
				ViewBag.Mensaje = ex.Message;
				return RedirectToAction("LogIn");
			}
		}
		public void CargarCategorias()
		{
			ViewBag.ListaCategorias = new SelectList(
						   (
							   from t in CategoriaController.CategoriaModel
							   select new SelectListItem
							   {
								   Text = t.nombre,
								   Value = t.idCat.ToString()
							   }
						   )
						   , "Value", "Text");
		}
		public void CargarGeneros()
		{
			ViewBag.ListaGenero = new SelectList(
						   (
							   from t in GeneroController.GeneroModel
							   select new SelectListItem
							   {
								   Text = t.genero,
								   Value = t.idGen.ToString()
							   }
						   )
						   , "Value", "Text");
		}
	}
}