﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburSisMVC.Models;
using Common;
using BRL;

namespace HamburSisMVC.Controllers
{
    public class ReservaController : Controller
    {
        // GET: Usuario
        public ActionResult ListReservas()
        {
			if (Session["ObjectSession"] != null)
			{
				Usuario usuario = Session["ObjectSession"] as Usuario;
				ReservaListModel _list;
				//llama a la vita de la lista mandandole la lista recuperada de un modelo
				if (usuario.Tipo == 0)
					 _list = ReservaListModel.GetReservasList();

				else
					 _list = ReservaListModel.GetReservasListUs(usuario.IdUsuario);//todos
				return View(_list);
				
			}
			else
				return RedirectToAction("LogIn", "LogIn");
		}
        // GET: Usuario/Create
        public ActionResult NewReserva()
        {
            CargarMesas();
            ReservaModel usm = new ReservaModel();
            return View(usm);//abre la vista de nuevo mandando un modelo vacio eso es obligatorio, ta que luego lo recupera pero lleno
        }
        // POST: Usuario/Create
        [HttpPost]
        public ActionResult NewReserva(ReservaModel usm)//el metodo post de nuevo, los post se deben llamar igual, varia sus parametros, en caso de este pide un modelo
        {
            try
            {
                // TODO: Add insert logic here
                Reserva res = new Reserva( usm.fechaReserva, usm.fechaExpiracion, 1, usm.idMesa);
                ReservaBRL.Insert(res);

                return RedirectToAction("ListReservas");//devuelve a la lista
            }
            catch
            {
                return RedirectToAction("ListReservas");
            }
        }
        // GET: Usuario/Edit/5
        public ActionResult EditReserva(int id)// pide un id para recuperar el dato
        {
            CargarMesas();
            ReservaModel us = ReservaModel.recuperar(id);//modelo que recupera el usuario
            return View(us);//llama a su vista cargando el resultado
        }
        // POST: Usuario/Edit/5
        [HttpPost]
        public ActionResult EditReserva(int id, ReservaModel usm)
        {
            //similar al post de insert
            try
            {
                Reserva res = new Reserva(id ,usm.fechaReserva, usm.fechaExpiracion, usm.activo,1, usm.idMesa);
                ReservaBRL.Update(res);

                return RedirectToAction("ListReservas");
            }
            catch
            {

                return RedirectToAction("ListReservas");
            }
        }
        // GET: Usuario/Delete/5
        public ActionResult Delete(int id) //contrlador que pide un id, no tiene vista propia
        {
            try
            {
                ReservaModel us = ReservaModel.recuperar(id);
                Reserva res = new Reserva(us.idReserva, us.fechaReserva, us.fechaExpiracion, us.activo, us.idUsuario, us.idMesa);
                ReservaBRL.Delete(res);
                return RedirectToAction("ListReservas");
            }
            catch
            {
                return RedirectToAction("ListReservas");
            }

        }
        public void CargarMesas()
        {
            ViewBag.ListaMesa = new SelectList(
                           (
                               from t in MesaListModel.GetMesaList()
                               select new SelectListItem
                               {
                                   Text = t.nroMesa.ToString(),
                                   Value = t.idMesa.ToString()
                               }
                           )
                           , "Value", "Text");
        }

    }
}