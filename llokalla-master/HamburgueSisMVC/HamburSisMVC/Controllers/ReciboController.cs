﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburSisMVC.Models;
using Common;
using BRL;
using CrystalDecisions.CrystalReports.Engine;

namespace HamburSisMVC.Controllers
{
    public class ReciboController : Controller
    {
        static List<int> ordenadas;
        // GET: Recibo
        public ActionResult NuevoRecibo()
        {
            if (ordenadas == null)
                ordenadas = new List<int>();
            ReciboModel rec = new ReciboModel();
            cargarOrdenes();
            enviarOrdenes();
            return View(rec);
        }
        [HttpPost]
        public ActionResult NuevoRecibo(ReciboModel rec)
        {
            try
            {
                if (rec.respaldo.idOrden == 0)
                {
                    Recibo or = new Recibo(rec.nit, rec.nombre);
                    or.Total = calcularTot();
                    ReciboBRL.Insert(or, ordenadas);
                    ordenadas = new List<int>();
                    return RedirectToAction("ListaOrdenes", "Orden");
                }
                else
                {
                    ordenadas.Add(rec.respaldo.idOrden);
                    cargarOrdenes();
                    enviarOrdenes();
                    return View();
                }
            }
            catch (Exception)
            {
                cargarOrdenes();
                enviarOrdenes();
                return View();
            }
        }

        private double calcularTot()
        {
            double total = 0;
            foreach (int item in ordenadas)
            {
                List<OrdenProducto> or =OrdenBRL.OrdenPedi(item);
                foreach (OrdenProducto item2 in or)
                {
                    total += item2.Cantidad * item2.Precio;
                }
            }
            return total;
        }

        public void cargarOrdenes()
        {
            ViewBag.Ordenes = new SelectList(
                           (
                               from t in OrdenListModel.GetOrdenes()
                               select new SelectListItem
                               {
                                   Text = t.idOrden.ToString(),
                                   Value = t.idOrden.ToString()
                               }
                           )
                           , "Value", "Text");
        }
        public void enviarOrdenes()
        {
            ViewBag.Tomadas = listasConver();
        }
        private List<OrdenModel> listasConver()
        {
            List<OrdenModel> res = new List<OrdenModel>();
            foreach (int item in ordenadas)
            {
                res.Add(new OrdenModel { idOrden=item });
            }
            return res;
        }
    }
}