﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburSisMVC.Models;
using Common;
using BRL;

namespace HamburSisMVC.Controllers
{
    public class OrdenController : Controller
    {
        static List<Common.OrdenProducto> parp;
        // GET: Orden
        public ActionResult ListaOrdenes()
        {
            OrdenListModel list=OrdenListModel.GetOrdenes();
            return View(list); 
        }
        public ActionResult Delete(int id) //contrlador que pide un id, no tiene vista propia
        {
            try
            {
                OrdenBRL.Delete(id);//borra un usuario
                return RedirectToAction("ListaOrdenes");
            }
            catch
            {
                return RedirectToAction("ListaOrdenes");
            }
        }
        public ActionResult NuevaOrden() //contrlador que pide un id, no tiene vista propia
        {
            cargarProductos();
            CargarMesas();
            if (parp == null)
            parp = new List<OrdenProducto>();
            rela();
            OrdenModel usm = new OrdenModel();
            usm.partes = listasConver();
            return View(usm);
        }
        [HttpPost]
        public ActionResult NuevaOrden(OrdenModel usm) //contrlador que pide un id, no tiene vista propia
        {
            usm.partes = new List<OrdenProductoModel>();
            try
            {

                if (usm.respaldo.cantidad==0)
                {
                    // TODO: Add insert logic here
                    Orden or = new Orden(1, usm.idMesa);
                    OrdenBRL.Insert(or,parp);
                    parp = new List<OrdenProducto>();
                    return RedirectToAction("ListaOrdenes");//devuelve a la lista
                }
                else
                {
                    Producto p = ProductoBRL.Get(usm.respaldo.idProducto);
                    OrdenProducto orp = new OrdenProducto(usm.respaldo.idProducto, usm.respaldo.cantidad, p.Precio);
                    parp.Add(orp);
                    usm.partes = listasConver();
                    cargarProductos();
                    CargarMesas();
                    rela();
                    return View();
                }

            }
            catch
            {
                cargarProductos();
                CargarMesas();
                rela();
                return View();
            }
        }
        public void cargarProductos()
        {
            ViewBag.ListaProdu = new SelectList(
                           (
                               from t in ProductoListModel.GetProductosList()
                               select new SelectListItem
                               {
                                   Text = t.nombreProducto.ToString(),
                                   Value = t.idProducto.ToString()
                               }
                           )
                           , "Value", "Text");
        }
        private List<OrdenProductoModel> listasConver()
        {
            List<OrdenProductoModel> res = new List<OrdenProductoModel>();
            foreach (OrdenProducto item in parp)
            {
                Producto p = ProductoBRL.Get(item.IdProducto);
                res.Add(new OrdenProductoModel { cantidad = item.Cantidad, precio = item.Precio,idProducto=item.IdProducto,nombre=p.NombreProducto });
            }
            return res;
        }
        public void CargarMesas()
        {
            ViewBag.ListaMesa = new SelectList(
                           (
                               from t in MesaListModel.GetMesaList()
                               select new SelectListItem
                               {
                                   Text = t.nroMesa.ToString(),
                                   Value = t.idMesa.ToString()
                               }
                           )
                           , "Value", "Text");
        }
        public void rela()
        {
            ViewBag.ordenes = listasConver();
        }
    }
}