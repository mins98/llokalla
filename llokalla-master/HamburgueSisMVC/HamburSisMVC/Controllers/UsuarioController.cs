﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburSisMVC.Models;
using BRL;
using Common;

namespace HamburSisMVC.Controllers
{
    public class UsuarioController : Controller
    {
        static List<Telefono> telfe;
        // GET: Usuario
        public ActionResult ListUser()
        {

			//llama a la vita de la lista mandandole la lista recuperada de un modelo
			if (Session["ObjectSession"] != null)
			{
				UsuarioListModel _list = UsuarioListModel.GetUsuarioModelList();
				return View(_list);
			}
			else
				return RedirectToAction("LogIn", "LogIn");
		}
       
        public ActionResult NewUser( )
        {
            CargarCategorias();
            CargarGeneros();
            if(telfe==null)
            telfe = new List<Telefono>();
            UsuarioModel usm = new Models.UsuarioModel();
            Telefonos();
            usm.Phonos = listasConver();
            return View(usm);//abre la vista de nuevo mandando un modelo vacio eso es obligatorio, ta que luego lo recupera pero lleno
        }
      
        [HttpPost]
        public ActionResult NewUser(UsuarioModel usm)//el metodo post de nuevo, los post se deben llamar igual, varia sus parametros, en caso de este pide un modelo
        {
            usm.Phonos = new List<TelefonoModel>();
            try
            {
                
                if (usm.tel.numero == null)
                {
                    // TODO: Add insert logic here
                    Usuario us = new Usuario(usm.userName, usm.password, usm.tipo);//crea un nuevo usuario con los resultados del modelo
                    Persona per = new Persona(usm.persona.nombre, usm.persona.primerAp, usm.persona.segundoAp, usm.persona.fechaNac, usm.persona.direccion, usm.persona.ci, usm.persona.genero);
                    UsuarioBRL.Insert(per, us, telfe);//usa el brl
                    telfe = new List<Telefono>();
                    return RedirectToAction("ListUser");//devuelve a la lista
                }
                else
                {
                    Telefono tel = new Telefono(usm.tel.numero, 1);
                    telfe.Add(tel);
                    usm.Phonos = listasConver();
                    CargarCategorias();
                    CargarGeneros();
                    Telefonos();
                    return View();
                }

            }
            catch
            {
                CargarCategorias();
                CargarGeneros();
                return View();
            }
        }
        // GET: Usuario/Edit/5
        public ActionResult EditUser(int id)// pide un id para recuperar el dato
        {
            CargarCategorias();
            CargarGeneros();
            UsuarioModel us =UsuarioModel.recuperar(id);//modelo que recupera el usuario
            ViewBag.Numeros = us.Phonos;
            return View(us);//llama a su vista cargando el resultado
        }
        // POST: Usuario/Edit/5
        [HttpPost]
        public ActionResult EditUser(int id,UsuarioModel usm)
        {
            //similar al post de insert
            try
            {
                Usuario us = new Usuario(id, usm.userName, usm.password, usm.activo, usm.tipo, usm.idPersona);
                Persona per = new Persona(usm.persona.idPersona,usm.persona.nombre,usm.persona.primerAp,usm.persona.segundoAp,usm.persona.fechaNac,usm.persona.direccion,usm.persona.ci,usm.persona.genero);
                List<Telefono> tel = new List<Telefono>();
                tel.Add(new Telefono(usm.tel.numero));
                UsuarioBRL.Update(per,us,tel);
                
                return RedirectToAction("ListUser");
            }
            catch
            {
                CargarCategorias();
                CargarGeneros();
                return View();
            }
        }
        // GET: Usuario/Delete/5
        public ActionResult Delete(int id) //contrlador que pide un id, no tiene vista propia
        {
            try
            {
                UsuarioBRL.Delete(id);//borra un usuario
                return RedirectToAction("ListUser");
            }
            catch
            {
                return RedirectToAction("ListUser");
            }
        }
        public void CargarCategorias()
        {
            ViewBag.ListaCategorias = new SelectList(
                           (
                               from t in CategoriaController.CategoriaModel
                               select new SelectListItem
                               {
                                   Text = t.nombre,
                                   Value = t.idCat.ToString()
                               }
                           )
                           , "Value", "Text");
        }
        public void CargarGeneros()
        {
            ViewBag.ListaGenero = new SelectList(
                           (
                               from t in GeneroController.GeneroModel
                               select new SelectListItem
                               {
                                   Text = t.genero,
                                   Value = t.idGen.ToString()
                               }
                           )
                           , "Value", "Text");
        }
        public void Telefonos()
        {
            ViewBag.Numeros = listasConver();
        }
        private List<TelefonoModel> listasConver()
        {
            List<TelefonoModel> res = new List<TelefonoModel>();
            foreach (Telefono item in telfe)
            {
                res.Add(new TelefonoModel { numero = item.Numero, tipo = item.Tipo });
            }
            return res;
        }
	}
}
