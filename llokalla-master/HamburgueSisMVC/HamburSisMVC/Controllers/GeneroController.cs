﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HamburSisMVC.Models;

namespace HamburSisMVC.Controllers
{
    public class GeneroController : Controller
    {
        private static List<GeneroModel> _GeneroModel;

        public static List<GeneroModel> GeneroModel
        {
            get
            {

                _GeneroModel = new List<GeneroModel>();

                _GeneroModel.Add(new GeneroModel
                {
                    idGen = 0,
                    genero = "Hombre"
                });
                _GeneroModel.Add(new GeneroModel
                {
                    idGen = 1,
                    genero = "Mujer"
                });
                _GeneroModel.Add(new GeneroModel
                {
                    idGen = 2,
                    genero = "Indefinido"
                });

                return _GeneroModel;
            }
            set { _GeneroModel = value; }
        }
        // GET: Genero
        public ActionResult Index()
        {
            return View();
        }
    }
}