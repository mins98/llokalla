﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BRL;

namespace HamburSisMVC.Models
{
    public class ProductoListModel : List<ProductoModel>
    {
        public static ProductoListModel GetProductosList()
        {

            ProductoListModel empleadoModelList = new ProductoListModel();
            foreach (var producto in ProductoBRL.Select())
            {
                empleadoModelList.Add(new ProductoModel
                {
                    idProducto = producto.IdProducto,
                    nombreProducto = producto.NombreProducto,
                    activo = producto.Activo,
                    descripcion = producto.Descripcion,
                    precio = producto.Precio,
                    tipo = producto.Tipo
                });
            }
            return empleadoModelList;
        }
    }
}