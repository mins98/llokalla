﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Common;
using BRL;

namespace HamburSisMVC.Models
{
    public class OrdenListModel:List<OrdenModel>
    {
            public static OrdenListModel GetOrdenes()
            {

                OrdenListModel empleadoModelList = new OrdenListModel();
                foreach (var producto in OrdenBRL.Select())
                {
                empleadoModelList.Add(new OrdenModel {
                    activo = producto.Activo,
                    fechaPedido = producto.FechaPedido,
                    idMesa = producto.IdMesa,
                    idOrden=producto.IdOrden,
                    idUsuario=producto.IdUsuario
                    });
                }
                return empleadoModelList;
            }
    }
}