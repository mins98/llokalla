﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using BRL;
using System.Web;

namespace HamburSisMVC.Models
{
    public class UsuarioModel
    {
        [Display(Name = "ID")]
        public int idUsuario
        {
            get;
            set;
        }
       //se supone que asi funciona pero no logro que de
        [Display(Name = "User", ResourceType = typeof(Resource.Resource))]
        public string userName
        {
            get;
            set;
        }
    
        [Display(Name = "Password", ResourceType = typeof(Resource.Resource))]
        public string password
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "RepeatPassword")]
		public string repeatPassword
		{
			get;
			set;
		}
		[Display(Name = "Activo")]
        public byte activo
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "Type", ResourceType = typeof(Resource.Resource))]
		public byte tipo
		{
			get;
			set;
		}
		[Display(Name = "captcha")]
        public byte captcha
        {
            get;
            set;
        }
        [Display(Name = "ID Persona")]
        public int idPersona
        {
            get;
            set;

        }
        public PersonaModel persona 
        {
            get;
            set;

        }
        public TelefonoModel tel
        {
            get;
            set;

        }

        public List<TelefonoModel> Phonos
        {
            get
            {
                return phonos;
            }

            set
            {
                phonos = value;
            }
        }

        private List<TelefonoModel> phonos;
        internal static UsuarioModel recuperar(int id)
        {
            Common.Usuario us = UsuarioBRL.getUsuario(id);
            Common.Persona p = UsuarioBRL.getPersona(us.IdPersona);
            List<Common.Telefono> tele = TelefonoBRL.listaTelefonos(id);
            List<TelefonoModel> phonos = new List<TelefonoModel>();
            foreach (Common.Telefono item in tele)
            {
                phonos.Add(new TelefonoModel { numero = item.Numero });
            }
            return new UsuarioModel
            {
                idUsuario = id,
                userName = us.UserName,
                password = us.Password,
				repeatPassword = us.SecondPassword,
				activo = us.Activo,
                tipo = us.Tipo,
                idPersona = us.IdPersona,
                persona = new PersonaModel
                {
                    idPersona = p.IdPersona,
                    nombre = p.Nombre,
                    primerAp = p.PrimerAp,
                    segundoAp = p.SegundoAp,
                    fechaNac = p.FechaNac,
                    direccion = p.Direccion,
                    ci = p.Ci,
                    genero = p.Genero,


                },
                Phonos=phonos
                
            };
        }
    }
}