﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HamburSisMVC.Models
{
    public class GeneroModel
    {
        [Display(Name = "ID")]
        public byte idGen
        {
            get;
            set;
        }
        [Display(Name = "Genero")]
        public string genero
        {
            get;
            set;
        }
    }
}