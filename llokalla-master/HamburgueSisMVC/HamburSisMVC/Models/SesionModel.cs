﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HamburSisMVC.Models
{
    public class SesionModel
    {
        [Display(Name = "ID")]
        private static int idUsuario
        {
            get;
            set;
        }
        [Display(Name = "ID Persona")]
        private static int idPersona
        {
            get;
            set;
        }
        [Display(Name = "CI")]
        private static string cI
        {
            get;
            set;
        }
        [Display(Name = "Nombre")]
        private static string name
        {
            get;
            set;
        }
        [Display(Name = "Apellido")]
        private static string pLastName
        {
            get;
            set;
        }
        [Display(Name = "Segundo Apellido")]
        private static string mLastName
        {
            get;
            set;
        }
        [Display(Name = "Fecha Nacimiento")]
        private static DateTime birthDay
        {
            get;
            set;
        }
        [Display(Name = "Direccion")]
        private static string adress
        {
            get;
            set;
        }
        [Display(Name = "Genero")]
        private static byte gender
        {
            get;
            set;
        }
        [Display(Name = "Nombre Usaurio")]
        private static string userName
        {
            get;
            set;
        }
        [Display(Name = "Rol")]
        private static byte role
        {
            get;
            set;
        }
        [Display(Name = "Activo")]
        private static string active
        {
            get;
            set;
        }
    }
}