﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HamburSisMVC.Models
{
    public class TelefonoModel
    {
        [Display(Name = "ID")]
        public int idTelefono
        {
            get;
            set;
        }
        [Display(Name = "idUsuario")]
        public int idUsuario
        {
            get;
            set;
        }
        [Display(Name = "Phone", ResourceType = typeof(Resource.Resource))]
        public string numero
        {
            get;
            set;
        }
        [Display(Name = "Tipo")]
        public byte tipo
        {
            get;
            set;
        }
        [Display(Name = "Activo")]
        public byte activo
        {
            get;
            set;
        }
    }
}