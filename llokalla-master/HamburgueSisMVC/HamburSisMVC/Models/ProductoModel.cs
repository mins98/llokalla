﻿using BRL;
using Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HamburSisMVC.Models
{
    public class ProductoModel
    {
        #region Propiedades

        /// <summary>
        /// Codigo
        /// </summary>
        [Display(Name = "ID")]
        public short idProducto
        {
            get;
            set;
        }


        [Display(Name = "Nombre")]
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [StringLength(30, ErrorMessageResourceName = "LogitudError", MinimumLength = 6, ErrorMessageResourceType = typeof(Resource.Resource))]
        public string nombreProducto
        {
            get;
            set;
        }
        [Display(Name = "Precio Producto")]
        public double precio
        {
            get;
            set;
        }
        [Display(Name = "Tipo")]
        public byte tipo
        {
            get;
            set;
        }
        [Display(Name = "Activo")]
        public byte activo
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "Descripcion")]
        [StringLength(60, ErrorMessageResourceName = "LogitudError", MinimumLength = 10, ErrorMessageResourceType = typeof(Resource.Resource))]
        public string descripcion
        {
            get;
            set;
        }

		internal static ProductoModel recuperar(byte id)
		{
			Producto producto = ProductoBRL.Get(id);
			return new ProductoModel
			{
				idProducto = producto.IdProducto,
				nombreProducto = producto.NombreProducto,
				precio = producto.Precio,
				tipo = producto.Tipo,
				activo = producto.Activo,
				descripcion = producto.Descripcion

			};
		}
		#endregion
	}
}