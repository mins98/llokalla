﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HamburSisMVC.Models
{
    public class ReciboModel
    {
        [Display(Name = "ID")]
        public int idRecibo
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "Nit")]
        public string nit
        {
            get;
            set;
        }
        [Display(Name = "Fecha")]
        public DateTime fecha
        {
            get;
            set;
        }
        [Display(Name = "Total")]
        public double total
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "N", ResourceType = typeof(Resource.Resource))]
        public string nombre
        {
            get;
            set;
        }
        [Display(Name = "SelecO", ResourceType = typeof(Resource.Resource))]
        public OrdenModel respaldo
        {
            get;
            set;
        }

    }
}