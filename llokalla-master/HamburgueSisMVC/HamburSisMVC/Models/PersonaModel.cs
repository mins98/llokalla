﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HamburSisMVC.Models
{
    public class PersonaModel
    {
        [Display(Name = "ID")]
        public int idPersona
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "N", ResourceType = typeof(Resource.Resource))]
        public string nombre
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "LastN", ResourceType = typeof(Resource.Resource))]
        public string primerAp
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "SLastName", ResourceType = typeof(Resource.Resource))]
        public string segundoAp
        {
            get;
            set;
        }


        [DataType(DataType.Date)]//esta wea es para que salga con formato date time picker
        public DateTime fechaNac
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "Direction", ResourceType = typeof(Resource.Resource))]
        public string direccion
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "Ci")]
        public string ci
        {
            get;
            set;
        }
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        [Display(Name = "Gender", ResourceType = typeof(Resource.Resource))]
        public byte genero
        {
            get;
            set;
        }
    }
}