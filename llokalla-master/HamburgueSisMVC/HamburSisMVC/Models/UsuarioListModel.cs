﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BRL;

namespace HamburSisMVC.Models
{
    public class UsuarioListModel : List<UsuarioModel>
    {
        public static UsuarioListModel GetUsuarioModelList()
        {

            UsuarioListModel empleadoModelList = new UsuarioListModel();
            foreach (var us in UsuarioBRL.listaUs())
            {
                empleadoModelList.Add(new UsuarioModel
                {
                    idUsuario = us.IdUsuario,
                    userName = us.UserName,

                });
            }
            return empleadoModelList;
        }

       
    }
}