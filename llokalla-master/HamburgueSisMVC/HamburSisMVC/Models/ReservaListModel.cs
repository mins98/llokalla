﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BRL;
using Common;

namespace HamburSisMVC.Models
{
    public class ReservaListModel:List<ReservaModel>
    {
        public static ReservaListModel GetReservasList()
        {

            ReservaListModel empleadoModelList = new ReservaListModel();
            foreach (var reserva in ReservaBRL.Select())
            {
                empleadoModelList.Add(new ReservaModel
                {
                    idReserva = reserva.IdReserva,
                    fechaReserva = reserva.FechaReserva,
                    fechaExpiracion=reserva.FechaExpiracion,
                    propietario = UsuarioBRL.getPersona(reserva.IdUsuario).Nombre + " " + UsuarioBRL.getPersona(reserva.IdUsuario).PrimerAp

                });
            }
            return empleadoModelList;
        }

        internal static ReservaListModel GetReservasListUs(int idUs)
        {
            ReservaListModel empleadoModelList = new ReservaListModel();
            foreach (var reserva in ReservaBRL.SelectUs(idUs))
            {
                empleadoModelList.Add(new ReservaModel
                {
                    idReserva = reserva.IdReserva,
                    fechaReserva = reserva.FechaReserva,
                    fechaExpiracion = reserva.FechaExpiracion,
                    propietario = UsuarioBRL.getPersona(reserva.IdUsuario).Nombre + " " + UsuarioBRL.getPersona(reserva.IdUsuario).PrimerAp

                });
            }
            return empleadoModelList;
        }
    }
}