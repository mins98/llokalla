﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Common;
using BRL;

namespace HamburSisMVC.Models
{
    public class ReservaModel
    {
        [Display(Name = "ID")]
        public int idReserva
        {
            get;
            set;
        }
        [Display(Name = "FechaR", ResourceType = typeof(Resource.Resource))]
        [DataType(DataType.Date)]
        public DateTime fechaReserva
        {
            get;
            set;
        }
        [Display(Name = "FechaE", ResourceType = typeof(Resource.Resource))]
        [DataType(DataType.Date)]
        public DateTime fechaExpiracion
        {
            get;
            set;
        }
        [Display(Name = "Activo")]
        public byte activo
        {
            get;
            set;
        }
        [Display(Name = "ID usuario")]
        public int idUsuario
        {
            get;
            set;
        }
        [Display(Name = "Mesa", ResourceType = typeof(Resource.Resource))]
        public byte idMesa
        {
            get;
            set;
        }
        [Display(Name = "Propietario", ResourceType = typeof(Resource.Resource))]
        public string propietario
        {
            get;
            set;
        }

        internal static ReservaModel recuperar(int id)
        {
            Reserva res = ReservaBRL.Get(id);
            ReservaModel re = new ReservaModel
            {
                idUsuario = res.IdUsuario,
                activo = res.Activo,
                fechaExpiracion = res.FechaExpiracion,
                fechaReserva = res.FechaReserva,
                idMesa=res.IdMesa,
                idReserva=res.IdReserva
            };
            return re;
        }
    }
}