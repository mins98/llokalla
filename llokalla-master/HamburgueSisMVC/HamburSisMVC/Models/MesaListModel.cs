﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BRL;

namespace HamburSisMVC.Models
{
    public class MesaListModel: List<MesaModel>
    {
    public static MesaListModel GetMesaList()
    {

        MesaListModel empleadoModelList = new MesaListModel();
        foreach (var mesa in MesaBRL.Select())
        {
            empleadoModelList.Add(new MesaModel
            {
                idMesa = mesa.IdMesa,
                nroMesa=mesa.NroMesa,
                ubicacion=mesa.Ubicacion,
                cantidadMaxima=mesa.CantidadMaxima,
                activo=mesa.Activo
            
            });
        }
        return empleadoModelList;
    }
}
}