﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Common;
using BRL;

namespace HamburSisMVC.Models
{
    public class MesaModel
    {
        #region Propiedades

        /// <summary>
        /// Codigo
        /// </summary>
        [Display(Name = "ID")]
        public byte idMesa
        {
            get;
            set;
        }
        [Display(Name = "N°")]
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        public short nroMesa
        {
            get;
            set;
        }
        [Display(Name = "Location", ResourceType = typeof(Resource.Resource))]
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        public string ubicacion
        {
            get;
            set;
        }
        [Display(Name = "Max", ResourceType = typeof(Resource.Resource))]
        [Required(ErrorMessageResourceName = "FieldRequired", ErrorMessageResourceType = typeof(Resource.Resource))]
        public byte cantidadMaxima
        {
            get;
            set;
        }
        [Display(Name = "Acivo")]
        public byte activo
        {
            get;
            set;
        }

        internal static MesaModel recuperar(byte id)
        {
            Mesa mes = MesaBRL.Get(id);
            return new MesaModel
            {
                idMesa = mes.IdMesa,
                nroMesa = mes.NroMesa,
                ubicacion = mes.Ubicacion,
                cantidadMaxima = mes.CantidadMaxima,
                activo = mes.Activo

            };
        }

        #endregion
    }
}