﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HamburSisMVC.Models
{
    public class OrdenModel
    {
        [Display(Name = "OrderN", ResourceType = typeof(Resource.Resource))]

        public int idOrden
        {
            get;
            set;
        }
        public int idUsuario
        {
            get;
            set;
        }
        [Display(Name = "Date", ResourceType = typeof(Resource.Resource))]
        [DataType(DataType.Date)]
        public DateTime fechaPedido
        {
            get;
            set;
        }
        [Display(Name = "ListM", ResourceType = typeof(Resource.Resource))]
        public byte idMesa
        {
            get;
            set;
        }
        public byte activo
        {
            get;
            set;
        }
        public int idRecibo
        {
            get;
            set;
        }

        public List<OrdenProductoModel> partes
        {
            get;
            set;
        }
        [Display(Name = "Producto a elegir")]
        public OrdenProductoModel respaldo
        {
            get;
            set;
        }
    }
}