﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HamburSisMVC.Models
{
    public class OrdenProductoModel
    {
        [Display(Name = "ID")]
        public int idOrden
        {
            get;
            set;
        }
        [Display(Name = "IDProducto")]
        public short idProducto
        {
            get;
            set;
        }
        [Display(Name = "Cant", ResourceType = typeof(Resource.Resource))]
        public byte cantidad
        {
            get;
            set;
        }
        [Display(Name = "Precio")]
        public double precio
        {
            get;
            set;
        }
        [Display(Name = "Producto")]
        public string nombre
        {
            get;
            set;
        }
    }
}