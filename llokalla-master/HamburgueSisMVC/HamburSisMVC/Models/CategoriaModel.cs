﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HamburSisMVC.Models
{
    public class CategoriaModel
    {
        [Display(Name = "ID")]
        public byte idCat
        {
            get;
            set;
        }
        [Display(Name = "Nombre")]
        public string nombre
        {
            get;
            set;
        }
    }
}