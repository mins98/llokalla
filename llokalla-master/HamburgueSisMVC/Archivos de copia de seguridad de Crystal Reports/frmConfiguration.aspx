﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmConfiguration.aspx.cs" Inherits="Visual.frmConfiguration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	<h1>Configuración</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	  
             <div class="contenedor-input">                  
				<asp:Button id="btnPerdiles" class="button button-block" Text="Perfiles" runat="server" onClick="btnPerdiles_Click"/>
            </div>
			 <div class="contenedor-input">                  
				<<asp:Button id="btnMesas" class="button button-block" Text="Mesas" runat="server" onClick="btnMesas_Click"/>
            </div>
      
	  
            <div class="contenedor-input">                  
				<asp:Button id="btnProductos" class="button button-block" Text="Productos" runat="server" onClick="btnProductos_Click"/>
            </div>
			 <div class="contenedor-input">                
				 <asp:Button id="btnReservas" class="button button-block" Text="Reservas" runat="server" onClick="btnReservas_Click"/>
            </div>
      
</asp:Content>
