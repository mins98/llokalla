﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmRecibo.aspx.cs" Inherits="Visual.frmRecibo" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	Recibo
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="contenedor-formulario">
		<!-- User -->
		<div id="productos">
			<!--Nombre y CI-->
            <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Lista de ordenes
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblOrdenes" runat="server" Width="709px" AutoPostBack="True"></asp:ListBox>
                </div>
                
            </div>
            <div class="contenedor-input">
							<asp:Button class="button button-not-block" text="Insertar" runat="server" onClick="btnInsetar_Click" UseSubmitBehavior="False"/>
				</div>
             <br />
                    <br />
                    <br />
             <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Ordenes para el recibo
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lbPedi" runat="server" Width="709px" AutoPostBack="True" ></asp:ListBox>
                </div>
            </div>
            <div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								NIT
							</label>
							<br/>
							<asp:TextBox id="txtNit" runat="server"/>
						</div>
						<div class="contenedor-input">
							<label>
								Nombre
							</label>
							<br/>
							<asp:TextBox runat="server" id="txtName"/>
						</div>
					</div>
          <div>
			<div class="contenedor-input">
				<asp:Button  class="button button-block" Text="Generar Recibo" runat="server"  UseSubmitBehavior="False" OnClick="btnRecibo_Click"  />
			</div>
		</div>
			</div>
	</div>
</asp:Content>
