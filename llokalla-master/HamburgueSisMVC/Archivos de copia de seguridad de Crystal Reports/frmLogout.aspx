﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmLogout.aspx.cs" Inherits="Visual.frmLogout" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	  <div class="contenedor-formulario">
		<div id="pregunta">
			<h1>Seguro que desea salir?</h1>
			<br/>
			<!--Botones-->
                <div class="contenedor-input">            
					<asp:Button  class="button button-block" UseSubmitBehavior="False" id="btnYes" OnClick="btnYes_Click" runat="server" Text="    Yes    "/>
				</div>
                <div class="contenedor-input">  
					<asp:Button  class="button button-block" UseSubmitBehavior="False" id="btnNo" OnClick="btnNo_Click" runat="server" Text="    No    "/>
				</div>
		  </div>
		</div>
</asp:Content>
