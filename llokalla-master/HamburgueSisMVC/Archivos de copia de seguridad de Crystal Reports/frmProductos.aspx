﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmProductos.aspx.cs" Inherits="Visual.frmProductos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	Productos
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- Formulario -->
	<div class="contenedor-formulario">
		<!-- User -->
		<div id="productos">
			<!--Nombre y CI-->
            <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Productos Disponibles
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblProductos" runat="server" Width="709px" AutoPostBack="True" OnSelectedIndexChanged="lblProductos_SelectedIndexChanged"></asp:ListBox>
            </div>
                    </div>
             <br />
                    <br />
                    <br />
            <div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Buscador
							</label>
							<br/>
							<asp:TextBox id="txtBusca" runat="server"/>
						</div>
					</div>
             <br />
                    <br />
                    <br />
              <div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Producto
							</label>
						</div>
				</div>
             <br />
                    <br />
                    <br />
              <div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Nombre
							</label>
							<br/>
							<asp:TextBox id="txtNombre" runat="server"/>
						</div>
                  <div class="contenedor-input">
							<label>
								Precio
							</label>
							<br/>
							<asp:TextBox id="txtPrecio" runat="server"/>
						</div>
					</div>
             <div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Descripccion
							</label>
							<br/>
							<asp:TextBox id="txtDescripcion" runat="server"/>
						</div>
                 <div class="contenedor-input">
							<label>
								Tipo de producto
							</label>
						</div>
						<div class="contenedor-input">
							<asp:ListBox id="lbxTipo" runat="server" Rows="3" SelectionMode="Single">
								<asp:ListItem Value="0">Bebida</asp:ListItem>
								<asp:ListItem Value="1">Comida</asp:ListItem>
								<asp:ListItem Value="2">Otro</asp:ListItem>
							</asp:ListBox>
						</div>

                 
					</div>
            <div>
			<div class="contenedor-input">
				<asp:Button  class="button button-block" Text="Insertar" runat="server" onClick="btnInsertar_Click" UseSubmitBehavior="False"  />
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Modificar" runat="server" OnClick="btnModificar_Click" UseSubmitBehavior="False" />
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Eliminar" runat="server" OnClick="btnEliminar_Click" UseSubmitBehavior="False" />
			</div>
		</div>
			</div>
	</div>
</asp:Content>
