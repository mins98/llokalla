﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmReserva.aspx.cs" Inherits="Visual.frmReserva" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	<h1>Reservas</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	 <!-- Formulario -->
	<div class="contenedor-formulario">
		<!-- Mesas -->
		<div id="mesas">
            <div  class="fila-arriba">
                <div class="contenedor-input">
                <label>
				Mesas disponibles
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblMesas" runat="server" OnSelectedIndexChanged="lblMesas_SelectedIndexChanged" Width="709px" AutoPostBack="True"></asp:ListBox>
            </div>
            </div>
			<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Número de Mesa
							</label>
							<br/>
							<asp:TextBox id="txtNroMesa" runat="server"/>
						</div>
						<div class="contenedor-input">
							<label>
								Ubicación
							</label>
							<br/>
							<asp:TextBox runat="server" id="txtUbicacion"/>
						</div>	
			</div>
			<div class="fila-arriba">
				<div class="contenedor-input">
					<label>
						Cantidad Máxima
					</label>
					<br/>
					<asp:TextBox runat="server" id="txtMaximo" />
				</div>
			</div>
            <br/>
			<div class="fila-arriba">
					<div class="contenedor-input">
							<label>
								Fecha de Atención
							</label>
							<br/>
                        <br/>
							<asp:Calendar id="calendarioExpiración" runat="server"/>
						</div>
			</div>
			
			<div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Reservar" runat="server" onClick="btnReservar_Click" UseSubmitBehavior="False"/>
			</div>
			</div>
		</div>
	</div>
</asp:Content>
