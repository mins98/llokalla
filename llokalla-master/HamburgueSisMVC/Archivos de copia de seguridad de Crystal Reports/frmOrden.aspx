﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmOrden.aspx.cs" Inherits="Visual.frmOrden" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	<h1>Ordenes</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <div class="contenedor-formulario">
		<!-- User -->
		<div id="productos">
			<!--Nombre y CI-->
            <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Mesas y Ordenes Pendientes
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblOrdenes" runat="server" Width="709px" AutoPostBack="True" OnSelectedIndexChanged="lblOrdenes_SelectedIndexChanged" ></asp:ListBox>
                </div>
            </div>
             <br />
                    <br />
                    <br />
             <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Productos del pedido
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblProOrd" runat="server" Width="709px" AutoPostBack="True" ></asp:ListBox>
                </div>
            </div>
          <div>
			<div class="contenedor-input">
				<asp:Button  class="button button-block" Text="Nueva" runat="server"  UseSubmitBehavior="False" OnClick="btnNuevo_Click"  />
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Atender" runat="server"  UseSubmitBehavior="False" OnClick="btnAtender_Click" />
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Eliminar" runat="server" UseSubmitBehavior="False" OnClick="btnEliminar_Click" />
			</div>
		</div>
			</div>
	</div>
</asp:Content>
