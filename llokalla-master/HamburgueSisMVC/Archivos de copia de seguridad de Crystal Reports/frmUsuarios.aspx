﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmUsuarios.aspx.cs" Inherits="Visual.frmUsuarios" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
    Usuarios
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
    <!-- Formulario -->
	<div class="contenedor-formulario">
		<!-- User -->
		<div id="usuario">
			<!--Nombre y CI-->
            <div  class="fila-arriba">
                <div class="contenedor-input">

            
                <label>
				Usuarios disponibles
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lbUsuarios" runat="server" OnSelectedIndexChanged="lbUsuarios_SelectedIndexChanged" Width="709px" AutoPostBack="True"></asp:ListBox>
            </div>
                    </div>
			<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								C.I.
							</label>
							<br/>
							<asp:TextBox id="txtCi" runat="server"/>
						</div>
						<div class="contenedor-input">
							<label>
								Nombre
							</label>
							<br/>
							<asp:TextBox runat="server" id="txtName"/>
						</div>
					</div>
			<!--Apellidos-->
			<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Primer Apellido
							</label>
							<br/>
							<asp:TextBox runat="server" id="txtFirstLastName" />
						</div>
						<div class="contenedor-input">
							<label>
								Segundo Apellido
							</label>
							<br/>
							<asp:TextBox runat="server" id="txtSecondLastName" />
						</div>
					</div>
			<!--Nombre Usuario-->
			<div class="contenedor-input">
						<label>
							Nombre de Usuario
						</label>
						<br/>
						<asp:TextBox runat="server" ID="txtuserNameR"/>
					</div>
			<!--Telefono-->
			<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Telefono
							</label>
							<br/>
							<asp:TextBox runat="server" id="txtTelefono"/>
						</div>
						<div class="contenedor-input">
							<asp:Button class="button button-not-block" text="Insertar" runat="server" onClick="btnTelefono_Click" UseSubmitBehavior="False"/>
						</div>
						<div class="contenedor-input">	
							<asp:ListBox id="lbxLista" runat="server" Rows="10"  Width="145px">

							</asp:ListBox>
						    <asp:ListBox ID="lbxLista2" runat="server" Height="194px" Width="180px"></asp:ListBox>
						</div>
					</div>
			<!--Contraseña 1-->
			<div class="contenedor-input">
						<label>
							Contraseña
						</label>
						<br/>
						<asp:TextBox runat="server" ID="txtPasswordR"/>
					</div>
			<!--Contraseña 2-->
			<div class="contenedor-input">
						<label>
							Repetir Contraseña  
						</label>
						<br/>
						<asp:TextBox runat="server" id="txtPasswordRTwo"/>
					</div>
			<!--Género-->
			<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Género
							</label>
						</div>
						<div class="contenedor-input">
							<asp:ListBox id="lbxGenero" runat="server" Rows="3" SelectionMode="Single">
								<asp:ListItem Value="0">Mujer</asp:ListItem>
								<asp:ListItem Value="1">Hombre</asp:ListItem>
								<asp:ListItem Value="2">Indefinido</asp:ListItem>
							</asp:ListBox>
						</div>
					</div>
			<br/>
			<!--Fecha Nacimineto-->
			<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Fecha Nacimiento  
							</label>
						</div>
						<br/>
						<div class="contenedor-input">
							<asp:Calendar runat="server" ID="cldDate"/>
						</div>
					</div>		
			<!--Dirección-->
			<div class="contenedor-input">
						<label>
							Dirección 
						</label>
						<br/>
						<asp:TextBox runat="server" ID="txtDireccion"/>
					</div>
			<!--Tipo-->
			<div class="fila-arriba">
				<div class="contenedor-input">
					<label>
						Tipo
					</label>
				</div>
				<div class="contenedor-input">
					<asp:ListBox id="lbxTipo" runat="server" Rows="3" SelectionMode="Single">
						<asp:ListItem Value="0">Administrador</asp:ListItem>
						<asp:ListItem Value="1">Empleado</asp:ListItem>
						<asp:ListItem Value="2">Cliente</asp:ListItem>
					</asp:ListBox>
				</div>
			</div>
		</div>
		<div >
			
		</div>
		<div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Insertar" runat="server" onClick="btnInsertar_Click" UseSubmitBehavior="False"/>
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Modificar" runat="server" onClick="btnModificar_Click" UseSubmitBehavior="False"/>
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Eliminar" runat="server" onClick="btnElimiar_Click" UseSubmitBehavior="False" />
			</div>
		</div>
	</div>
</asp:Content>
