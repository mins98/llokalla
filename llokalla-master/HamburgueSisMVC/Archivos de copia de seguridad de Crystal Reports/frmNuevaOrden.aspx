﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmNuevaOrden.aspx.cs" Inherits="Visual.frmNuevaOrden" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
	<h1>Nueva Orden</h1>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
     <div class="contenedor-formulario">
		<!-- User -->
		<div id="productos">
			<!--Nombre y CI-->
            <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Lista de mesas
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblMesas" runat="server" Width="709px" AutoPostBack="True"></asp:ListBox>
                </div>
                
            </div>
             <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Productos
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblProducto" runat="server" Width="709px" AutoPostBack="True"></asp:ListBox>
                </div>
                <div class="contenedor-input">
							<label>
								Cantidad
							</label>
							<br/>
							<asp:TextBox id="txtCant" runat="server"/>
						</div>
                 <div class="contenedor-input">
							<asp:Button class="button button-not-block" text="Insertar" runat="server" onClick="btnIngres_Click" UseSubmitBehavior="False"/>
				</div>
            </div>
             <div  class="fila-arriba">
                <div class="contenedor-input">            
                <label>
				Pedidos
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblagregados" runat="server" Width="709px" AutoPostBack="True"></asp:ListBox>
                    <asp:ListBox ID="lblCantidad" runat="server" Width="709px" AutoPostBack="True" Visible="False"></asp:ListBox>
                      <asp:ListBox ID="lblId" runat="server" Width="709px" AutoPostBack="True" Visible="False"></asp:ListBox>
                </div>
              	
            </div>
			<div class="contenedor-input">
				<asp:Button  class="button button-block" Text="Ordenar" runat="server"  UseSubmitBehavior="False" OnClick="btnOrdnar_Click"  />
			</div>
		</div>
	</div>
</asp:Content>
