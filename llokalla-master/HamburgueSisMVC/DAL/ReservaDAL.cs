﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public static class ReservaDAL
    {
        public static void Delete(Reserva res)
        {
            List<SqlCommand> comandos = new List<SqlCommand>();
            string query = "UPDATE Reserva SET activo=0 WHERE idReserva=@idReserva";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idReserva", res.IdReserva);
                comandos.Add(cmd);
                query = "UPDATE Mesa SET activo=1 WHERE idMesa=@idMesa";
                cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idMesa", res.IdMesa);
                comandos.Add(cmd);
                Metodos.ExecuteMultiCommand(comandos);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Delete de reserva exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static List<Reserva> SelectUs(int id)
        {
            List<Reserva> res = new List<Reserva>();
            string query = "SELECT idReserva,  fechaReserva,fechaExpiracion,idUsuario FROM Reserva WHERE activo=1 AND idUsuario=@idUsuario";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idUsuario", id);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add(new Reserva(int.Parse(dr[0].ToString()), DateTime.Parse(dr[1].ToString()), DateTime.Parse(dr[2].ToString()), int.Parse(dr[3].ToString())));
                }
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de reservas exitoso.", Sesion.IdUsuario));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }

        public static void Insert(Reserva reserva)
        {
            List<SqlCommand> comandos=new List<SqlCommand>();
            string query = "INSERT INTO Reserva(fechaReserva,fechaExpiracion,idMesa,idUsuario) VALUES(@fechaReserva,@fechaExpiracion,@idMesa,@idUsuario)";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@fechaReserva", reserva.FechaReserva);
                cmd.Parameters.AddWithValue("@fechaExpiracion", reserva.FechaExpiracion);
                cmd.Parameters.AddWithValue("@idMesa", reserva.IdMesa);
                cmd.Parameters.AddWithValue("@idUsuario", reserva.IdUsuario);
                comandos.Add(cmd);
                query = "UPDATE Mesa SET activo=2 WHERE idMesa=@idMesa";
                cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idMesa", reserva.IdMesa);
                comandos.Add(cmd);
                Metodos.ExecuteMultiCommand(comandos);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Insert de reservas exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static List<Reserva> Select()
        {
            List<Reserva> res = new List<Reserva>();
            string query = "SELECT idReserva, fechaReserva,fechaExpiracion,idUsuario FROM Reserva WHERE activo=1";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add(new Reserva(int.Parse(dr[0].ToString()),DateTime.Parse(dr[1].ToString()),DateTime.Parse(dr[2].ToString()),int.Parse(dr[3].ToString())));
                }
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de reservas exitoso.", Sesion.IdUsuario));
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
        
        public static void Update(Reserva reserva)
        {
            string query = "UPDATE Reserva SET fechaReserva=@fechaReserva,fechaExpiracion=@fechaExpiracion,idMesa=@idMesa,idUsuario=@idUsuario WHERE idReserva=@idReserva";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@fechaReserva", reserva.FechaReserva);
                cmd.Parameters.AddWithValue("@fechaExpiracion", reserva.FechaExpiracion);
                cmd.Parameters.AddWithValue("@idMesa", reserva.IdMesa);
                cmd.Parameters.AddWithValue("@idUsuario", reserva.IdUsuario);
                cmd.Parameters.AddWithValue("@idReserva", reserva.IdReserva);
                Metodos.ExecuteBasicCommand(cmd);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Update de reservas exitosa", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static Reserva Get(int idReserva)
        {
            Reserva res = null;
            string query = "SELECT  idReserva, fechaReserva, fechaExpiracion, activo, idUsuario, idMesa FROM Reserva WHERE idReserva=@idReserva";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idReserva", idReserva);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res = new Reserva(int.Parse(dr[0].ToString()), DateTime.Parse(dr[1].ToString()), DateTime.Parse(dr[2].ToString()), byte.Parse(dr[3].ToString()), int.Parse(dr[4].ToString()), byte.Parse(dr[5].ToString()));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get de reserva exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
    }
}
