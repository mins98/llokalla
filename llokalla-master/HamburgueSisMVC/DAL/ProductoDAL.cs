﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;
using System.Data;

namespace DAL
{
    public static class ProductoDAL 
    {
        public static void Delete(short idProducto)
        {
            string query = "UPDATE Producto SET activo=0 WHERE idProducto=@idProducto";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idProducto", idProducto);
                Metodos.ExecuteBasicCommand(cmd);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Delete de Pruducto exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static void Insert(Producto pr)
        {
            string query = "INSERT INTO Producto(nombre,precio,tipo,descripcion) VALUES(@nombre,@precio,@tipo,@descripcion)";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@nombre", pr.NombreProducto);
                cmd.Parameters.AddWithValue("@precio", pr.Precio);
                cmd.Parameters.AddWithValue("@tipo", pr.Tipo);
                cmd.Parameters.AddWithValue("@descripcion", pr.Descripcion);
                Metodos.ExecuteBasicCommand(cmd);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Insert de producto exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static List<Producto> Select()
        {
            List<Producto> res = new List<Producto>();
            string query = "SELECT * FROM Producto WHERE activo=1";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add(new Producto(short.Parse(dr[0].ToString()), dr[4].ToString(), double.Parse(dr[1].ToString()), byte.Parse(dr[5].ToString()), byte.Parse(dr[3].ToString()), dr[2].ToString()));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de productos exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
        public static void Update(Producto pr)
        {
            string query = "UPDATE Producto SET nombre=@nombre,precio=@precio,tipo=@tipo,descripcion=@descripcion WHERE idProducto=@idProducto";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idProducto", pr.IdProducto);
                cmd.Parameters.AddWithValue("@nombre", pr.NombreProducto);
                cmd.Parameters.AddWithValue("@precio", pr.Precio);
                cmd.Parameters.AddWithValue("@tipo", pr.Tipo);
                cmd.Parameters.AddWithValue("@descripcion", pr.Descripcion);
                Metodos.ExecuteBasicCommand(cmd);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Update de producto exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static Producto Get(short idProducto)
        {
            Producto res = null;
            string query = "SELECT idProducto,nombre,precio,tipo,activo,descripcion FROM Producto WHERE idProducto=@idProducto";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idProducto", idProducto);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res = new Producto(byte.Parse(dr[0].ToString()), dr[1].ToString(), double.Parse(dr[2].ToString()), byte.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()), dr[5].ToString());
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get de producto exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
    }
}
