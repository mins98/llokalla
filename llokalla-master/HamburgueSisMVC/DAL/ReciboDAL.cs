﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public static class ReciboDAL 
    {

        public static int ultimoId()
        {
            int res=2;
            string query = "SELECT MAX(idRecibo) FROM Recibo";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res = int.Parse(dr[0].ToString());
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Último ID de recdo exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
        public static void Insert(Recibo recibo, List<int> ordenes)
        {
            List<SqlCommand> comands = new List<SqlCommand>();
            string query = "INSERT INTO Recibo (nit,total,nombre) VALUES (@nit,@total,@nombre) ";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@nit", recibo.Nit);
                cmd.Parameters.AddWithValue("@total", recibo.Total);
                cmd.Parameters.AddWithValue("@nombre",recibo.Nombre);
                comands.Add(cmd);
                int id = Metodos.GetIDGenerateTable("Recibo");
                foreach (int item in ordenes)
                {
                    query = "UPDATE Orden SET idRecibo=@idRecibo , activo=2 WHERE idOrden=@idOrden";
                    cmd = Metodos.CreateBasicComand(query);
                    cmd.Parameters.AddWithValue("@idOrden", item);
                    cmd.Parameters.AddWithValue("@idRecibo", id);
          
                    comands.Add(cmd);
                }

                Metodos.ExecuteMultiCommand(comands);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Insert de Recibo exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static DataTable Select()
        {
            DataTable tabla = new DataTable();
            string query = "SELECT * FROM Recibo";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                tabla = Metodos.ExecuteDataTableCommand(cmd);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de recibo exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            return tabla;
        }
    }
}
