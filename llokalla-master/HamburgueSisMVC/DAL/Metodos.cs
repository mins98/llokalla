﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace DAL
{
    public static class Metodos
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["BDSenetConnectionString"].ConnectionString;

        public static SqlCommand CreateBasicComand()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            return cmd;
        }

        public static SqlCommand CreateBasicComand(string query)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = connection;
            return cmd;
        }
        #region Ejecuciones de comandos
        /// <summary>
        /// Ejecuta un comando
        /// </summary>
        /// <param name="cmd">Comando a ejecutar</param>
        public static void ExecuteBasicCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        /// <summary>
        /// Ejecuta una lista de ocmandos en una transaccion
        /// </summary>
        /// <param name="comands">Lista de comandos</param>
        public static void ExecuteMultiCommand(List<SqlCommand> comands)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlTransaction tran = null;
            try
            {

                connection.Open();
                tran = connection.BeginTransaction();
                foreach (SqlCommand item in comands)
                {
                    item.Connection = connection;
                    item.Transaction = tran;
                    item.ExecuteNonQuery();
                }
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// Devuelve una tabla, resultado de un comando de select
        /// </summary>
        /// <param name="cmd">Comando a ejecutar</param>
        /// <returns>DataTable con datos de la consulta</returns>
        public static DataTable ExecuteDataTableCommand(SqlCommand cmd)
        {
            DataTable res = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(res);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
        /// <summary>
        /// Decualve un data adapter de una consulta
        /// </summary>
        /// <param name="cmd">Comando a ejecutar</param>
        /// <returns>Data Adapter de la consulta</returns>
        public static SqlDataReader ExecuteDataReaderComand(SqlCommand cmd)
        {
            SqlDataReader res = null;
            try
            {
                cmd.Connection.Open();
                res = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        /// <summary>
        /// Devuelve el numero de filas de una consulta
        /// </summary>
        /// <param name="cmd">Comando a ejecutar</param>
        /// <returns>Nuemro de filas</returns>
        public static byte ExecuteDataReaderRows(SqlCommand cmd)
        {
            byte con = 0;
            SqlDataReader dr = null;
            dr = Metodos.ExecuteDataReaderComand(cmd);
            while (dr.Read())
            {
                con++;
            }
            return con;
        }
        /// <summary>
        /// Devuelve el id siguiente aun no creado de una tabla
        /// </summary>
        /// <param name="tabla">Tabla de la cual se buscara el id</param>
        /// <returns>El numero del id nuevo</returns>
        public static int GetIDGenerateTable(string tabla)
        {
            int res = -1;
            string query = "SELECT IDENT_CURRENT('" + tabla + "')+IDENT_INCR('" + tabla + "')";

            try
            {
                SqlCommand cmd = CreateBasicComand(query);
                res = int.Parse(ExecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        /// <summary>
        /// Devuelve el mayor id de una tabla
        /// </summary>
        /// <param name="id">Nombre de la columna de id</param>
        /// <param name="tabla">Tabla en la cual se buscara</param>
        /// <returns>El numero del mayor id</returns>
        public static int GetIDTable(string id, string tabla)
        {
            int res = -1;
            string query = "SELECT MAX(" + id + ") FROM " + tabla;

            try
            {
                SqlCommand cmd = CreateBasicComand(query);
                res = int.Parse(ExecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        /// <summary>
        /// Ejecuta un comando escalar
        /// </summary>
        /// <param name="cmd">Comando a ejecutar</param>
        /// <returns>El resultado del comando</returns>
        public static string ExecuteScalarCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        #endregion
    }
}
