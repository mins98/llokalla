﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;

namespace DAL
{
    public static class UsuarioDAL 
    {
      
        public static void Delete(int idUsuario)
        {
            List<SqlCommand> comands = new List<SqlCommand>();
            string query = "UPDATE Usuario SET activo=0 WHERE idUsuario=@idUsuario";

            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                comands.Add(cmd);
                query = "UPDATE Telefono SET activo=0 WHERE idUsuario=@idUsuario";
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                comands.Add(cmd);
                Metodos.ExecuteMultiCommand(comands);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Delete de usuario exitoso.", Sesion.IdUsuario));

			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static void InsertTemporal(Usuario us)
        {
            SqlCommand cmd;
            try
            {
                string query = "INSERT INTO Usuario(userName, password, tipo,idPersona) VALUES(@userName, HASHBYTES('md5', @password), @tipo,@idPersona)";
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@userName", us.UserName);
                cmd.Parameters.AddWithValue("@password", us.Password).SqlDbType = SqlDbType.VarChar; ;
                cmd.Parameters.AddWithValue("@tipo", us.Tipo);
                cmd.Parameters.AddWithValue("@idPersona", 1);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void UpdateTemporal(Usuario us)
        {
            SqlCommand cmd;
            try
            {
                string query = "UPDATE Usuario SET userName=@userName,password=HASHBYTES('md5', @password),tipo=@tipo  WHERE idUsuario=@idUsuario";
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@userName", us.UserName);
                cmd.Parameters.AddWithValue("@password", us.Password).SqlDbType = SqlDbType.VarChar; ;
                cmd.Parameters.AddWithValue("@tipo", us.Tipo);
                cmd.Parameters.AddWithValue("@idUsuario", us.IdUsuario);
                Metodos.ExecuteBasicCommand(cmd);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void Insert(Persona per,Usuario us,List<Telefono> fonos)
        {
            List<SqlCommand> comands = new List<SqlCommand>();
            string query;
            try
            {
                query = "INSERT INTO Persona (nombre,primerApellido,segundoApellido,fechaNacimiento,direccion,ci,genero) VALUES (@nombre,@primerApellido,@segundoApellido,@fechaNacimiento,@direccion,@ci,@genero)";
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@nombre",per.Nombre);
                cmd.Parameters.AddWithValue("@primerApellido", per.PrimerAp);
                cmd.Parameters.AddWithValue("@segundoApellido", per.SegundoAp);
                cmd.Parameters.AddWithValue("@fechaNacimiento",per.FechaNac);
                cmd.Parameters.AddWithValue("@direccion", per.Direccion);
                cmd.Parameters.AddWithValue("@ci", per.Ci);
                cmd.Parameters.AddWithValue("@genero", per.Genero);
                comands.Add(cmd);
                int id = Metodos.GetIDGenerateTable("Persona");
                query = "INSERT INTO Usuario(userName, password, tipo,idPersona) VALUES(@userName, HASHBYTES('md5', @password), @tipo,@idPersona)";
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@userName", us.UserName);
                cmd.Parameters.AddWithValue("@password", us.Password).SqlDbType = SqlDbType.VarChar; ;
                cmd.Parameters.AddWithValue("@tipo", us.Tipo);
                cmd.Parameters.AddWithValue("@idPersona", id);
                comands.Add(cmd);
                id = Metodos.GetIDGenerateTable("Usuario");
                foreach (Telefono item in fonos)
                {
                    query = "INSERT INTO Telefono (numero,tipo,idUsuario) VALUES (@numero,@tipo,@idUsuario) ";
                    cmd = Metodos.CreateBasicComand(query);
                    cmd.Parameters.AddWithValue("@numero", item.Numero);
                    cmd.Parameters.AddWithValue("@tipo", item.Tipo);
                    cmd.Parameters.AddWithValue("@idUsuario", id);
                    comands.Add(cmd);
                }
                Metodos.ExecuteMultiCommand(comands);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Inserción de usuarios exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static DataTable Select()
        {
            DataTable tabla = new DataTable();
            string query = "SELECT * FROM Usuario";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                tabla = Metodos.ExecuteDataTableCommand(cmd);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de usuarios exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            return tabla;
        }
		public static int SelectUserID(string usuario, string password)
		{
			int resultado = -1;
			string query = @"SELECT U.idUsuario 
							FROM Usuario U
							WHERE U.userName = @userName AND U.password = HASHBYTES('md5', @password) AND U.activo = 1";
			SqlCommand command = null;
			SqlDataReader dataReader = null;
			try
			{
				command = Metodos.CreateBasicComand(query);
				command.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;
				command.Parameters.AddWithValue("@userName", usuario);

				dataReader = Metodos.ExecuteDataReaderComand(command);
				while (dataReader.Read())
				{
					resultado = int.Parse(dataReader[0].ToString());
					if (resultado != -1)
						break;
				}
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get IdUsuario exitoso.", Sesion.IdUsuario));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
			}
			finally
			{
				command.Connection.Close();
				dataReader.Close();
			}
			return resultado;
		}

		public static bool GetUser(string userName)
		{
			string query = "SELECT idUsuario FROM Usuario WHERE userName=@userName";
			SqlCommand command = null;
			try
			{
				command = Metodos.CreateBasicComand();
				command.CommandText = query;
				command.Parameters.AddWithValue("@userName", userName);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Comprobar usuario exitoso.", Sesion.IdUsuario));
				return (Metodos.ExecuteDataTableCommand(command).Rows.Count == 0);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
			}
			finally
			{
				command.Connection.Close();
			}
		}
		public static List<Usuario> listaUs ()
        {

            List< Usuario> res = new List<Usuario>();
            string query = "SELECT idUsuario,userName,tipo FROM Usuario WHERE activo=1";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add ( new Usuario(int.Parse(dr[0].ToString()), dr[1].ToString()));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de Usuarios exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {

				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }

        public static void Update(Persona per, Usuario us, List<Telefono> fonos)
        {
            List<SqlCommand> comands = new List<SqlCommand>();
            string query = "UPDATE Usuario SET userName=@userName,password=HASHBYTES('md5', @password),tipo=@tipo  WHERE idUsuario=@idUsuario";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@userName", us.UserName);
                cmd.Parameters.AddWithValue("@password", us.Password).SqlDbType = SqlDbType.VarChar; ;
                cmd.Parameters.AddWithValue("@tipo", us.Tipo);
                cmd.Parameters.AddWithValue("@idUsuario", us.IdUsuario);
                comands.Add(cmd);
                query = "UPDATE Persona SET nombre=@nombre,primerApellido=@primerApellido,segundoApellido=@segundoApellido,fechaNacimiento=@fechaNacimiento,direccion=@direccion,ci=@ci,genero=@genero WHERE idPersona=@idPersona";
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@nombre", per.Nombre);
                cmd.Parameters.AddWithValue("@primerApellido", per.PrimerAp);
                cmd.Parameters.AddWithValue("@segundoApellido", per.SegundoAp);
                cmd.Parameters.AddWithValue("@fechaNacimiento", per.FechaNac);
                cmd.Parameters.AddWithValue("@direccion", per.Direccion);
                cmd.Parameters.AddWithValue("@ci", per.Ci);
                cmd.Parameters.AddWithValue("@genero", per.Genero);
                cmd.Parameters.AddWithValue("@idPersona", per.IdPersona);
                comands.Add(cmd);
                foreach (Telefono item in fonos)
                {
                    query = "UPDATE Telefono SET numero=@numero WHERE idUsuario=@idUsuario";
                    cmd = Metodos.CreateBasicComand(query);
                    cmd.Parameters.AddWithValue("@numero", item.Numero);
                    cmd.Parameters.AddWithValue("@idUsuario", us.IdUsuario);
                    comands.Add(cmd);
                }
				Metodos.ExecuteMultiCommand(comands);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Update usuario exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static Usuario getUsuario(int idUsuario)
        {
            Usuario res = null;
            string query = "SELECT idUsuario,userName,password,activo,tipo,idPersona FROM Usuario WHERE idUsuario=@idUsuario";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res = new Usuario(int.Parse( dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), byte.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()), int.Parse(dr[5].ToString()));
				}
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get Usuarios exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
		public static int getIdPersona(string cI)
		{
			int resultado = -1;
			string query = "SELECT idPersona FROM Persona WHERE cI=@cI";
			SqlCommand command = null;
			SqlDataReader dataReader = null;
			try
			{
				command = Metodos.CreateBasicComand(query);
				command.Parameters.AddWithValue("@cI", cI);
				dataReader = Metodos.ExecuteDataReaderComand(command);
				while (dataReader.Read())
				{
					resultado = int.Parse( dataReader[0].ToString());
					if (resultado != -1)
						break;
				}
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get de IdUsuario exitoso.", Sesion.IdUsuario));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
			}
			finally
			{
				command.Connection.Close();
				dataReader.Close();
			}
			return resultado;
		}

		public static bool getUsuario(string cI)
		{
			string query = "SELECT idPersona,nombre,primerApellido,segundoApellido,fechaNacimiento,direccion,ci,genero FROM Persona WHERE ci=@cI";
			SqlCommand command = null;
			try
			{
				command = Metodos.CreateBasicComand();
				command.CommandText = query;
				command.Parameters.AddWithValue("@cI", cI);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Comprobar usuario exitoso.", Sesion.IdUsuario));
				return (Metodos.ExecuteDataTableCommand(command).Rows.Count > 0);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
			}
			finally
			{
				command.Connection.Close();
			}
		}

		public static Persona getPersona(int idPersona)
        {
            Persona res = null;
            string query = "SELECT idPersona,nombre,primerApellido,segundoApellido,fechaNacimiento,direccion,ci,genero FROM Persona WHERE idPersona=@idPersona";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idPersona", idPersona);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res = new Persona(int.Parse(dr[0].ToString()), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(),DateTime.Parse(dr[4].ToString()), dr[5].ToString(), dr[6].ToString(),byte.Parse(dr[7].ToString()));
                }

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get persona exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }

		public static DataTable Login(string usuario, string password)
		{
			string query = @"SELECT U.idUsuario, U.userName, U.activo, U.tipo, U.idPersona, P.ci, P.nombre, P.primerApellido, ISNULL(P.segundoApellido, ''), P.fechaNacimiento, P.direccion, P.genero 
							FROM Usuario U 
							INNER JOIN Persona P ON P.idPersona = U.idPersona 
							WHERE U.userName=@userName AND U.password=HASHBYTES('md5',@password) AND U.activo=1";
			SqlCommand command = null;
			try
			{
				command = Metodos.CreateBasicComand();
				command.CommandText = query;
				command.Parameters.AddWithValue("@userName", usuario);
				command.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;


				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "LogIn exitoso.", Sesion.IdUsuario));
				return Metodos.ExecuteDataTableCommand(command);
			}
			catch (SqlException exception)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), exception.Message, Sesion.IdUsuario));
				throw exception; 
			}
			catch (Exception exception)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), exception.Message, Sesion.IdUsuario));
				throw exception;
			}
		}        
    }
}
