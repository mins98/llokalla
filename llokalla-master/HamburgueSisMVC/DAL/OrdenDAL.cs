﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public static class OrdenDAL 
    {
        public static void Delete(int idOrden)
        {
            List<SqlCommand> comands = new List<SqlCommand>();
            string query = "UPDATE Orden SET activo=0 WHERE idOrden=@idOrden";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idOrden", idOrden);
                comands.Add(cmd);
                query = "DELETE FROM OrdenProducto WHERE idOrden=@idOrden";
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idOrden", idOrden);
                comands.Add(cmd);
                Metodos.ExecuteMultiCommand(comands);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Delete de orden exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

		public static Orden Get(string selectedValue)
		{
				Orden res = null;
				string query = "SELECT idOrden, idMesa FROM  Orden WHERE idOrden=@idOrden";
				SqlCommand cmd = null;
				SqlDataReader dr = null;
				try
				{
					cmd = Metodos.CreateBasicComand(query);
					cmd.Parameters.AddWithValue("@idOrden", int.Parse(selectedValue));
					dr = Metodos.ExecuteDataReaderComand(cmd);
					while (dr.Read())
					{
						res = new Orden(int.Parse(dr[0].ToString()),byte.Parse(dr[1].ToString()));
					}
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get de orden exitoso.", Sesion.IdUsuario));
			}
				catch (Exception ex)
				{
					System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
					throw ex;
				}
				finally
				{
					cmd.Connection.Close();
					dr.Close();
				}
				return res;

		}

		public static void Insert(Orden orden,List<OrdenProducto> items)
        {
            List<SqlCommand> comands = new List<SqlCommand>();
            string query = "INSERT INTO Orden(idUsuario,idMesa) VALUES(@idUsuario,@idMesa)";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idUsuario", orden.IdUsuario);
                cmd.Parameters.AddWithValue("@idMesa", orden.IdMesa);
                comands.Add(cmd);
                int id = Metodos.GetIDGenerateTable("Orden");
                foreach (OrdenProducto item in items)
                {
                    query = "INSERT INTO OrdenProducto (idProducto,idOrden,cantidad,precio) VALUES(@idProducto,@idOrden,@cantidad,@precio)";
                    cmd = Metodos.CreateBasicComand(query);
                    cmd.Parameters.AddWithValue("@idProducto", item.IdProducto);
                    cmd.Parameters.AddWithValue("@idOrden", id);
                    cmd.Parameters.AddWithValue("@cantidad", item.Cantidad);
                    cmd.Parameters.AddWithValue("@precio", item.Precio);
                    comands.Add(cmd);
                }
                
                Metodos.ExecuteMultiCommand(comands);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Insert de Orden exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static List<Orden> Select()
        {
            List<Orden> res = new List<Orden>();
            DataTable tabla = new DataTable();
            string query = "SELECT * FROM Orden WHERE activo=1";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add(new Orden(int.Parse(dr[0].ToString()),int.Parse(dr[1].ToString()),DateTime.Parse(dr[2].ToString()),byte.Parse(dr[3].ToString()),byte.Parse(dr[4].ToString())));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de orden exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }

        public static void Update(int idOrden)
        {
            string query = "UPDATE Orden SET activo=2 WHERE idOrden=@idOrden";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idReserva", idOrden);
                Metodos.ExecuteBasicCommand(cmd);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Update de orden exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static void Atendido(int idOrden)
        {
            string query = "UPDATE Orden SET activo=2 WHERE idOrden=@idOrden";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idOrden", idOrden);
                Metodos.ExecuteBasicCommand(cmd);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Ordne tomada exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static List<OrdenProducto> OrdenPedi(int idOrden)
        {
            List<OrdenProducto> res = new List<OrdenProducto>();
            DataTable tabla = new DataTable();
            string query = "SELECT * FROM OrdenProducto WHERE idOrden=@idOrden";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idOrden", idOrden);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add(new OrdenProducto(short.Parse(dr[0].ToString()), int.Parse(dr[1].ToString()), byte.Parse(dr[2].ToString()), double.Parse(dr[3].ToString())));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de Orden pedido exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
    }
}
