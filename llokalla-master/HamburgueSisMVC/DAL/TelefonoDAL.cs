﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public static class TelefonoDAL
    {
        public static Telefono getTelefono(int idUsuario)
        {
            Telefono res = null;
            string query = "SELECT idTelefofo,idUsuario,numero,tipo,activo FROM Telefono WHERE idUsuario=@idUsuario";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res = new Telefono(int.Parse(dr[0].ToString()), int.Parse(dr[1].ToString()), dr[2].ToString(), byte.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get telefonos exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
        public static List<Telefono> listaTelefonos(int idUsuario)
        {
            List<Telefono> tabla = new List<Telefono>();
            SqlDataReader dr = null;
            string query = "SELECT idTelefofo ,numero  FROM Telefono WHERE idUsuario=@idUsuario AND activo=1";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    tabla.Add(new Telefono(int.Parse(dr[0].ToString()), dr[1].ToString()));
                }

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get lista de teléfonos exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            return tabla;
        }
        public static void insertarTelefonosNuevos(List<Telefono> cel, int idUsuario)
        {
            List<SqlCommand> comands = new List<SqlCommand>();
            string query;
			try
			{
				foreach (Telefono item in cel)
				{
					query = "INSERT INTO Telefono (numero,tipo,idUsuario) VALUES (@numero,@tipo,@idUsuario) ";
					SqlCommand cmd = Metodos.CreateBasicComand(query);
					cmd = Metodos.CreateBasicComand(query);
					cmd.Parameters.AddWithValue("@numero", item.Numero);
					cmd.Parameters.AddWithValue("@tipo", item.Tipo);
					cmd.Parameters.AddWithValue("@idUsuario", idUsuario);
					comands.Add(cmd);
				}
				Metodos.ExecuteMultiCommand(comands);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Insert de teléfonos exitoso.", Sesion.IdUsuario));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw;
			}
		}
        public static void eliminarTelefono(int idTelefono)
        {
            string query = "UPDATE Telefono SET activo = 0 WHERE idTelefofo =@idTelefofo";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idTelefofo", idTelefono);
                Metodos.ExecuteBasicCommand(cmd);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Delete de teléfonos exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
    }
}
