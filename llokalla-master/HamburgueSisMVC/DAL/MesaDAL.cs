﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using System.Data.SqlClient;

namespace DAL
{
    public static class MesaDAL 
    {
        public static void Delete(byte idMesa)
        {
            string query = "UPDATE Mesa SET activo=0 WHERE idMesa=@idMesa";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idMesa", idMesa);
                Metodos.ExecuteBasicCommand(cmd);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Delete de mesa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static List<Mesa> SelectReserva()
        {
            List<Mesa> res = new List<Mesa>();
            DataTable tabla = new DataTable();
            string query = "SELECT * FROM Mesa WHERE activo=2";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add(new Mesa(byte.Parse(dr[0].ToString()),short.Parse(dr[1].ToString()), dr[2].ToString(), byte.Parse(dr[3].ToString()),byte.Parse(dr[4].ToString())));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select mesas reservadas.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
        public static void Desocupar(byte idMesa)
        {
            string query = "UPDATE Mesa SET activo=1 WHERE idMesa=@idMesa";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idMesa", idMesa);
                Metodos.ExecuteBasicCommand(cmd);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Desecupado de mesa exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static void Insert(Mesa mesa)
        {
            string query = "INSERT INTO Mesa(nroMesa,ubicacion,cantidadMaxima) VALUES(@nroMesa,@ubicacion,@cantidadMaxima)";
            try
            {
                SqlCommand cmd = DAL.Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@nroMesa", mesa.NroMesa);
                cmd.Parameters.AddWithValue("@ubicacion",mesa.Ubicacion);
                cmd.Parameters.AddWithValue("@cantidadMaxima", mesa.CantidadMaxima);
                Metodos.ExecuteBasicCommand(cmd);

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Insert de mesas exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }

        public static List<Mesa> Select()
        {
            List<Mesa> res = new List<Mesa>();
            DataTable tabla = new DataTable();
            string query = "SELECT * FROM Mesa WHERE activo=1";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res.Add(new Mesa(byte.Parse(dr[0].ToString()), short.Parse(dr[1].ToString()), dr[2].ToString(), byte.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString())));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Select de mesa exitoso.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
        public static void Update(Mesa mesa)
        {
            string query = "UPDATE Mesa SET nroMesa=@nroMesa ,ubicacion=@ubicacion,cantidadMaxima=@cantidadMaxima WHERE idMesa=@idMesa";
            try
            {
                SqlCommand cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@nroMesa", mesa.NroMesa);
                cmd.Parameters.AddWithValue("@ubicacion", mesa.Ubicacion);
                cmd.Parameters.AddWithValue("@cantidadMaxima", mesa.CantidadMaxima);
                cmd.Parameters.AddWithValue("@idMesa", mesa.IdMesa);
                Metodos.ExecuteBasicCommand(cmd);
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Update de mesa exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
        }
        public static Mesa Get(byte idMesa)
        {
            Mesa res = null;
            string query = "SELECT idMesa,nroMesa,ubicacion,cantidadMaxima,activo FROM Mesa WHERE idMesa=@idMesa";
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            try
            {
                cmd = Metodos.CreateBasicComand(query);
                cmd.Parameters.AddWithValue("@idMesa", idMesa);
                dr = Metodos.ExecuteDataReaderComand(cmd);
                while (dr.Read())
                {
                    res = new Mesa(byte.Parse(dr[0].ToString()), short.Parse(dr[1].ToString()), dr[2].ToString(), byte.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()));
                }
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get mesa exitosa.", Sesion.IdUsuario));
			}
            catch (Exception ex)
            {
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                dr.Close();
            }
            return res;
        }
		public static bool GetByNumber(string number)
		{
			string query = "SELECT idMesa FROM Mesa WHERE nroMesa=@nroMesa";
			SqlCommand command = null;
			try
			{
				command = Metodos.CreateBasicComand();
				command.CommandText = query;
				command.Parameters.AddWithValue("@nroMesa", byte.Parse(number));

				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get de mesa exitosa.", Sesion.IdUsuario));

				return (Metodos.ExecuteDataTableCommand(command).Rows.Count > 0);
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
			}
			finally
			{
				command.Connection.Close();
			}
		}
		public static Mesa GetTableByNumber(string number)
		{
			Mesa res = null;
			string query = "SELECT idMesa,nroMesa,ubicacion,cantidadMaxima,activo FROM Mesa WHERE number=@number";
			SqlCommand cmd = null;
			SqlDataReader dr = null;
			try
			{
				cmd = Metodos.CreateBasicComand(query);
				cmd.Parameters.AddWithValue("@number", short.Parse(number));
				dr = Metodos.ExecuteDataReaderComand(cmd);
				while (dr.Read())
				{
					res = new Mesa(byte.Parse(dr[0].ToString()), short.Parse(dr[1].ToString()), dr[2].ToString(), byte.Parse(dr[3].ToString()), byte.Parse(dr[4].ToString()));
				}
				System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), "Get mesa exitosa.", Sesion.IdUsuario));
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine(string.Format("{0} Info: {1} Evento {2}", DateTime.Now.ToLongDateString(), ex.Message, Sesion.IdUsuario));
				throw ex;
			}
			finally
			{
				cmd.Connection.Close();
				dr.Close();
			}
			return res;
		}
	}
}
