﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Data;
using Common;

namespace BRL
{
    public class ProductoBRL
    {
        public static void Delete(short idProducto)
        {
            ProductoDAL.Delete(idProducto);
        }

        public static void Insert(Producto pr)
        {
            ProductoDAL.Insert(pr);
        }

        public static List<Producto> Select()
        {
            return ProductoDAL.Select();
        }
        public static void Update(Producto pr)
        {
            ProductoDAL.Update(pr);
        }
        public static Producto Get(short idProducto)
        {
            return ProductoDAL.Get(idProducto);
        }
    }
}
