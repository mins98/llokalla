﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;
using System.Data;

namespace BRL
{
    public static class MesaBRL
    {
        public static void Delete(byte idMesa)
        {
            MesaDAL.Delete(idMesa);
        }

        public static void Insert(Mesa mesa)
        {
            MesaDAL.Insert(mesa);
        }

        public static List<Mesa> Select()
        {
            return MesaDAL.Select();
        }
        public static void Update(Mesa mesa)
        {
            MesaDAL.Update(mesa);
        }
        public static Mesa Get(byte idMesa)
        {
            return MesaDAL.Get(idMesa);
        }
		public static bool GetByNumber(string number)
		{
			return MesaDAL.GetByNumber(number);
		}
		public static Mesa GetTableByNumber(string number)
		{
			return MesaDAL.GetTableByNumber(number);
		}

		public static void Desocupar(byte idMesa)
        {
            MesaDAL.Desocupar(idMesa);
        }
        public static List<Mesa> SelectReserva()
        {
            return MesaDAL.SelectReserva();
        }
    }
}
