﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;
using System.Data;

namespace BRL
{
    public static class ReciboBRL
    {
        public static void Insert(Recibo recibo, List<int> ordenes)
        {
            ReciboDAL.Insert(recibo, ordenes);
        }

        public static DataTable Select()
        {
            return ReciboDAL.Select();
        }
        public static int ultimoId()
        {
            return ReciboDAL.ultimoId();
        }
    }
}
