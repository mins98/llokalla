﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Common;
using DAL;

namespace BRL
{
	public static class UsuarioBRL 
	{
		public static bool GetUser(string userName)
		{
			return UsuarioDAL.GetUser(userName);
		}
		public static int SelectUserID(string usuario, string password)
		{
			return UsuarioDAL.SelectUserID(usuario, password);
		}
		public static void Delete(int idUsuario)
		{
            UsuarioDAL.Delete(idUsuario);
		}
        public static List<Usuario> listaUs()
        {
            return UsuarioDAL.listaUs();
        }

        public static void Insert(Persona per,Usuario us, List<Telefono> fonos)
		{
            UsuarioDAL.Insert(per, us, fonos);
		}

		public static DataTable Select()
		{
            return UsuarioDAL.Select();
		}

		public static void Update(Persona per, Usuario us, List<Telefono> fonos)
		{
            UsuarioDAL.Update(per, us, fonos);
        }
		public static Usuario getUsuario(int idUsuario)
		{
			return UsuarioDAL.getUsuario(idUsuario);
		}

		public static bool getPersonByCi(string cI)
		{
			return UsuarioDAL.getUsuario(cI);
		}

		public static Persona getPersona(int idPersona)
		{
			return UsuarioDAL.getPersona(idPersona);
		}
		
		public static DataTable Login(string user, string password)
		{
			return UsuarioDAL.Login(user, password);
		}
        public static int getIdPersona(string cI)
        {
            return UsuarioDAL.getIdPersona(cI);
        }

        public static void InsertTemporal(Usuario us)
        {
            UsuarioDAL.InsertTemporal(us);
        }

        public static void UpdateTemp(Usuario us)
        {
            UsuarioDAL.UpdateTemporal(us);
        }
    }
}