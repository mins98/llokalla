﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;

namespace BRL
{
    public static class TelefonoBRL
    {
        public static void eliminarTelefono(int idTelefono)
        {
            TelefonoDAL.eliminarTelefono(idTelefono);
        }

        public static void insertarTelefonosNuevos(List<Telefono> cel, int idUsuario)
        {
            TelefonoDAL.insertarTelefonosNuevos(cel, idUsuario);
        }
        public static List<Telefono> listaTelefonos(int idUsuario)
        {
            return TelefonoDAL.listaTelefonos(idUsuario);
        }
        public static Telefono getTelefono(int idTelefono)
        {
            return TelefonoDAL.getTelefono(idTelefono);

        }
    }
}
