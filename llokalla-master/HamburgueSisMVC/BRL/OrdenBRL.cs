﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common;
using DAL;

namespace BRL
{
    public static class OrdenBRL
    {
        public static void Delete(int idOrden)
        {
            OrdenDAL.Delete(idOrden);
        }

        public static void Insert(Orden orden, List<OrdenProducto> items)
        {
            OrdenDAL.Insert(orden, items);
        }

        public static List<Orden> Select()
        {
            return OrdenDAL.Select();
        }

        public static void Update(int idOrden)
        {
            OrdenDAL.Update(idOrden);
        }
        public static void Atendido(int idOrden)
        {
            OrdenDAL.Atendido(idOrden);
        }
        public static List<OrdenProducto> OrdenPedi(int idOrden)
        {
            return OrdenDAL.OrdenPedi(idOrden);
        }

		public static Orden Get(string selectedValue)
		{
			return OrdenDAL.Get(selectedValue);
		}
	}
}
