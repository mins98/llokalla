﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Common;
using System.Data;

namespace BRL
{
    public static class ReservaBRL
    {
        public static void Delete(Reserva res)
        {
            ReservaDAL.Delete(res);
        }

        public static void Insert(Reserva reserva)
        {
            ReservaDAL.Insert(reserva);
        }

        public static List<Reserva> Select()
        {
            return ReservaDAL.Select();
        }

        public static void Update(Reserva reserva)
        {
            ReservaDAL.Update(reserva);
        }
        public static Reserva Get(int idReserva)
        {
            return ReservaDAL.Get(idReserva);
        }

        public static List<Reserva> SelectUs(int id)
        {
            return ReservaDAL.SelectUs(id);
        }
    }
}
