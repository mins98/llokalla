﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
	public static class Sesion
	{
		private static int idUsuario;
		private static int idPersona;
		private static string cI;
		private static string name;
		private static string pLastName;
		private static string mLastName;
		private static DateTime birthDay;
		private static string adress;
		private static byte gender;
		private static string userName;
		private static byte role;
		private static string active;

        public static int IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public static int IdPersona
        {
            get
            {
                return idPersona;
            }

            set
            {
                idPersona = value;
            }
        }

        public static string CI
        {
            get
            {
                return cI;
            }

            set
            {
                cI = value;
            }
        }

        public static string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public static string PLastName
        {
            get
            {
                return pLastName;
            }

            set
            {
                pLastName = value;
            }
        }

        public static string MLastName
        {
            get
            {
                return mLastName;
            }

            set
            {
                mLastName = value;
            }
        }

        public static DateTime BirthDay
        {
            get
            {
                return birthDay;
            }

            set
            {
                birthDay = value;
            }
        }

        public static string Adress
        {
            get
            {
                return adress;
            }

            set
            {
                adress = value;
            }
        }

        public static byte Gender
        {
            get
            {
                return gender;
            }

            set
            {
                gender = value;
            }
        }

        public static string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public static byte Role
        {
            get
            {
                return role;
            }

            set
            {
                role = value;
            }
        }

        public static string Active
        {
            get
            {
                return active;
            }

            set
            {
                active = value;
            }
        }

        //public static int IdUsuario { get => idUsuario; set => idUsuario = value; }
        //public static int IdPersona { get => idPersona; set => idPersona = value; }
        //public static string CI { get => cI; set => cI = value; }
        //public static string Name { get => name; set => name = value; }
        //public static string PLastName { get => pLastName; set => pLastName = value; }
        //public static string MLastName { get => mLastName; set => mLastName = value; }
        //public static DateTime BirthDay { get => birthDay; set => birthDay = value; }
        //public static string Adress { get => adress; set => adress = value; }
        //public static byte Gender { get => gender; set => gender = value; }
        //public static string UserName { get => userName; set => userName = value; }
        //public static string Role { get => role; set => role = value; }
        //public static string Active { get => active; set => active = value; }
    }
}
