﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Producto
    {
        private short idProducto;
        private string nombreProducto;
        private double precio;
        private byte tipo;
        private byte activo;
        private string descripcion;

        public short IdProducto
        {
            get
            {
                return idProducto;
            }

            set
            {
                idProducto = value;
            }
        }

        public string NombreProducto
        {
            get
            {
                return nombreProducto;
            }

            set
            {
                nombreProducto = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public byte Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
                tipo = value;
            }
        }

        public byte Activo
        {
            get
            {
                return activo;
            }

            set
            {
                activo = value;
            }
        }

        public string Descripcion
        {
            get
            {
                return descripcion;
            }

            set
            {
                descripcion = value;
            }
        }
        public Producto(string nombreProducto, double precio, byte tipo,  string descripcion)
        {
            this.nombreProducto = nombreProducto;
            this.precio = precio;
            this.tipo = tipo;
            this.descripcion = descripcion;
        }
        public Producto(short idProducto, string nombreProducto, double precio, byte tipo, byte activo, string descripcion)
        {
            this.idProducto = idProducto;
            this.nombreProducto = nombreProducto;
            this.precio = precio;
            this.tipo = tipo;
            this.activo = activo;
            this.descripcion = descripcion;
        }
    }
}
