﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class OrdenProducto
    {
        private int idOrden;
        private short idProducto;
        private byte cantidad;
        private double precio;

        public int IdOrden
        {
            get
            {
                return idOrden;
            }

            set
            {
                idOrden = value;
            }
        }

        public short IdProducto
        {
            get
            {
                return idProducto;
            }

            set
            {
                idProducto = value;
            }
        }

        public byte Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }
        public OrdenProducto(short idProducto,int idOrden, byte cantidad, double precio)
        {
            this.idOrden = idOrden;
            this.idProducto = idProducto;
            this.cantidad = cantidad;
            this.precio = precio;
        }
        public OrdenProducto(short idProducto, byte cantidad, double precio)
        {
            this.idProducto = idProducto;
            this.cantidad = cantidad;
            this.precio = precio;
        }
    }
}
