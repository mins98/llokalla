﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Usuario
    {
        private int idUsuario;
        private string userName;
        private string password;
		private string secondPassword;
		private byte activo;
        private byte tipo;
        private int idPersona;

        public int IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public string UserName
        {
            get
            {
                return userName;
            }

            set
            {
                userName = value;
            }
        }

        public string Password
        {
            get
            {
                return password;
            }

            set
            {
                password = value;
            }
        }

        public byte Activo
        {
            get
            {
                return activo;
            }

            set
            {
                activo = value;
            }
        }

        public byte Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
                tipo = value;
            }
        }

        public int IdPersona
        {
            get
            {
                return idPersona;
            }

            set
            {
                idPersona = value;
            }
        }

		public string SecondPassword
		{
			get
			{
				return secondPassword;
			}

			set
			{
				secondPassword = value;
			}
		}

		public Usuario(string userName, string password, byte tipo)
        {
            this.userName = userName;
            this.password = password;
            this.tipo = tipo;
        }
        public Usuario(int idUsuario, string userName, string password, byte activo, byte tipo,int idPersona)
        {
            this.idUsuario = idUsuario;
            this.userName = userName;
            this.password = password;
            this.activo = activo;
            this.tipo = tipo;
            this.idPersona = idPersona;
        }
		public Usuario(string userName, string password, string secondPassword, byte tipo)
		{
			this.userName = userName;
			this.password = password;
			this.secondPassword = secondPassword;
			this.tipo = tipo;
		}

		public Usuario()
		{
		}
        public Usuario(int idUsuario,string userName)
        {
            this.userName = userName;
            this.idUsuario = idUsuario;
        }
    }
}
