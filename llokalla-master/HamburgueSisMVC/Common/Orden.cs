﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Orden
    {
        private int idOrden;
        private int idUsuario;
        private DateTime fechaPedido;
        private byte idMesa;
        private byte activo;
        private int idRecibo;

        public int IdOrden
        {
            get
            {
                return idOrden;
            }

            set
            {
                idOrden = value;
            }
        }

        public int IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public DateTime FechaPedido
        {
            get
            {
                return fechaPedido;
            }

            set
            {
                fechaPedido = value;
            }
        }

        public byte IdMesa
        {
            get
            {
                return idMesa;
            }

            set
            {
                idMesa = value;
            }
        }

        public byte Activo
        {
            get
            {
                return activo;
            }

            set
            {
                activo = value;
            }
        }

        public int IdRecibo
        {
            get
            {
                return idRecibo;
            }

            set
            {
                idRecibo = value;
            }
        }

      

        public Orden( int idUsuario, DateTime fechaPedido, byte idMesa, byte activo, int idRecibo)
        {
            this.idUsuario = idUsuario;
            this.fechaPedido = fechaPedido;
            this.idMesa = idMesa;
            this.activo = activo;
        }
        public Orden(int idOrden, int idUsuario, DateTime fechaPedido, byte idMesa, byte activo, int idRecibo)
        {
            this.idOrden = idOrden;
            this.idUsuario = idUsuario;
            this.fechaPedido = fechaPedido;
            this.idMesa = idMesa;
            this.activo = activo;
            this.idRecibo = idRecibo;
        }
        public Orden(int idOrden, int idUsuario, DateTime fechaPedido, byte idMesa, byte activo)
        {
            this.idOrden = idOrden;
            this.idUsuario = idUsuario;
            this.fechaPedido = fechaPedido;
            this.idMesa = idMesa;
            this.activo = activo;
        }
        public Orden(int idOrden, int idUsuario, byte idMesa, byte activo)
        {
            this.idOrden = idOrden;
            this.idUsuario = idUsuario;
            this.idMesa = idMesa;
            this.activo = activo;
        }
        public Orden( int idUsuario, byte idMesa)
        {
            this.idUsuario = idUsuario;
            this.idMesa = idMesa;
        }
    }
}
