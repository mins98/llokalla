﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Mesa
    {
        private byte idMesa;
        private short nroMesa;
        private string ubicacion;
        private byte cantidadMaxima;
        private byte activo;
        public byte IdMesa
        {
            get
            {
                return idMesa;
            }

            set
            {
                idMesa = value;
            }
        }

        public short NroMesa
        {
            get
            {
                return nroMesa;
            }

            set
            {
                nroMesa = value;
            }
        }

        public string Ubicacion
        {
            get
            {
                return ubicacion;
            }

            set
            {
                ubicacion = value;
            }
        }

        public byte CantidadMaxima
        {
            get
            {
                return cantidadMaxima;
            }

            set
            {
                cantidadMaxima = value;
            }
        }

        public byte Activo
        {
            get
            {
                return activo;
            }

            set
            {
                activo = value;
            }
        }

        public Mesa(byte idMesa, short nroMesa, string ubicacion, byte cantidadMaxima, byte activo)
        {
            this.idMesa = idMesa;
            this.nroMesa = nroMesa;
            this.ubicacion = ubicacion;
            this.cantidadMaxima = cantidadMaxima;
            this.activo = activo;
        }
        public Mesa(short nroMesa, string ubicacion, byte cantidadMaxima)
        {
            this.nroMesa = nroMesa;
            this.ubicacion = ubicacion;
            this.cantidadMaxima = cantidadMaxima;
        }

    }
}
