﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Telefono
    {
        private int idTelefono;
        private int idUsuario;
        private string numero;
        private byte tipo;
        private byte activo;

        public int IdTelefono
        {
            get
            {
                return idTelefono;
            }

            set
            {
                idTelefono = value;
            }
        }

        public int IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public string Numero
        {
            get
            {
                return numero;
            }

            set
            {
                numero = value;
            }
        }

        public byte Tipo
        {
            get
            {
                return tipo;
            }

            set
            {
				tipo = (byte)((numero.Length == 8) ? 0 : 1);
            }
        }

        public byte Activo
        {
            get
            {
                return activo;
            }

            set
            {
                activo = value;
            }
        }

        public Telefono(int idTelefono, int idUsuario, string numero, byte tipo)
        {
            this.IdTelefono = idTelefono;
            this.IdUsuario = idUsuario;
            this.Numero = numero;
            this.Tipo = tipo;
        }
        public Telefono(string numero)
        {
            this.Numero = numero;
        }
        public Telefono(int idTelefono, string numero, byte tipo,byte activo)
        {
            this.IdTelefono = idTelefono;
            this.Numero = numero;
            this.Tipo = tipo;
            this.activo = activo;
        }
        public Telefono(int idTelefono, int idUsuario, string numero, byte tipo, byte activo)
        {
            this.IdTelefono = idTelefono;
            this.IdUsuario = idUsuario;
            this.Numero = numero;
            this.Tipo = tipo;
            this.activo = activo;
        }
        public Telefono(int idTelefono, string numero)
        {
            this.IdTelefono = idTelefono;
            this.Numero = numero;
        }
        public Telefono( string numero, byte tipo)
        {
            this.tipo = tipo;
            this.numero = numero;
        }
    }
}
