﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Recibo
    {
        private int idRecibo;
        private string nit;
        private DateTime fecha;
        private double total;
        private string nombre;

        public int IdRecibo
        {
            get
            {
                return idRecibo;
            }

            set
            {
                idRecibo = value;
            }
        }

        public string Nit
        {
            get
            {
                return nit;
            }

            set
            {
                nit = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public Recibo(string nit, string nombre)
        {
            this.nit = nit;
            this.nombre = nombre;
        }

        public Recibo(int idRecibo, string nit, DateTime fecha, double total, string nombre)
        {
            this.idRecibo = idRecibo;
            this.nit = nit;
            this.fecha = fecha;
            this.total = total;
            this.nombre = nombre;
        }
    }
}
