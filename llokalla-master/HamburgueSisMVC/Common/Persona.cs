﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Persona
    {
        private int idPersona;
        private string nombre;
        private string primerAp;
        private string segundoAp;
        private DateTime fechaNac;
        private string direccion;
        private string ci;
        private byte genero;

        public int IdPersona
        {
            get
            {
                return idPersona;
            }

            set
            {
                idPersona = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }

        public string PrimerAp
        {
            get
            {
                return primerAp;
            }

            set
            {
                primerAp = value;
            }
        }

        public string SegundoAp
        {
            get
            {
                return segundoAp;
            }

            set
            {
                segundoAp = value;
            }
        }

        public DateTime FechaNac
        {
            get
            {
                return fechaNac;
            }

            set
            {
                fechaNac = value;
            }
        }

        public string Direccion
        {
            get
            {
                return direccion;
            }

            set
            {
                direccion = value;
            }
        }

        public string Ci
        {
            get
            {
                return ci;
            }

            set
            {
                ci = value;
            }
        }

        public byte Genero
        {
            get
            {
                return genero;
            }

            set
            {
                genero = value;
            }
        }
        public Persona(int idPersona, string nombre, string primerAp, string segundoAp, DateTime fechaNac, string direccion, string ci, byte genero)
        {
            this.idPersona = idPersona;
            this.nombre = nombre;
            this.primerAp = primerAp;
            this.segundoAp = segundoAp;
            this.fechaNac = fechaNac;
            this.direccion = direccion;
            this.ci = ci;
            this.genero = genero;
        }
        public Persona(string nombre, string primerAp, string segundoAp, DateTime fechaNac, string direccion, string ci, byte genero)
        {
            this.nombre = nombre;
            this.primerAp = primerAp;
            this.segundoAp = segundoAp;
            this.fechaNac = fechaNac;
            this.direccion = direccion;
            this.ci = ci;
            this.genero = genero;
        }

		public Persona()
		{
		}
	}
}
