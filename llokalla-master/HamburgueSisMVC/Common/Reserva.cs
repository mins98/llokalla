﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class Reserva
    {
        private int idReserva;
        private DateTime fechaReserva;
        private DateTime fechaExpiracion;
        private byte activo;
        private int idUsuario;
        private byte idMesa;
        private short v;


        public int IdReserva
        {
            get
            {
                return idReserva;
            }

            set
            {
                idReserva = value;
            }
        }

        public DateTime FechaReserva
        {
            get
            {
                return fechaReserva;
            }

            set
            {
                fechaReserva = value;
            }
        }

        public DateTime FechaExpiracion
        {
            get
            {
                return fechaExpiracion;
            }

            set
            {
                fechaExpiracion = value;
            }
        }

        public byte Activo
        {
            get
            {
                return activo;
            }

            set
            {
                activo = value;
            }
        }

        public int IdUsuario
        {
            get
            {
                return idUsuario;
            }

            set
            {
                idUsuario = value;
            }
        }

        public byte IdMesa
        {
            get
            {
                return idMesa;
            }

            set
            {
                idMesa = value;
            }
        }
        public Reserva(DateTime fechaReserva, DateTime fechaExpiracion, int idUsuario, byte idMesa)
        {
            this.fechaReserva = fechaReserva;
            this.fechaExpiracion = fechaExpiracion;
            this.idUsuario = idUsuario;
            this.idMesa = idMesa;
        }

        public Reserva(int idReserva, DateTime fechaReserva, DateTime fechaExpiracion, byte activo, int idUsuario, byte idMesa)
        {
            this.idReserva = idReserva;
            this.fechaReserva = fechaReserva;
            this.fechaExpiracion = fechaExpiracion;
            this.activo = activo;
            this.idUsuario = idUsuario;
            this.idMesa = idMesa;
        }
        public Reserva(int idReserva, DateTime fechaReserva, DateTime fechaExpiracion, int idUsuario)
        {
            this.idReserva = idReserva;
            this.fechaReserva = fechaReserva;
            this.fechaExpiracion = fechaExpiracion;
            this.idUsuario = idUsuario;
        }
        public Reserva(int idReserva, DateTime fechaReserva)
        {
            this.idReserva = idReserva;
            this.fechaReserva = fechaReserva;
        }

        public Reserva(int id, DateTime fechaReserva, byte activo, int idUsuario, byte idMesa)
        {
            this.idReserva = id;
            this.fechaReserva = fechaReserva;
            this.activo = activo;
            this.idUsuario = idUsuario;
            this.idMesa = idMesa;
        }
    }
}
