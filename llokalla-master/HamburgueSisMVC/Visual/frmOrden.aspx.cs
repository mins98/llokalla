﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BRL;
using Common;

namespace Visual
{
    public partial class frmOrden : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargar();
        }
        public void cargar()
        {
            if (!Page.IsPostBack)
            {
                lblOrdenes.DataSource = OrdenBRL.Select();
                lblOrdenes.DataValueField = "idOrden";
                lblOrdenes.DataTextField = "idOrden";
                lblOrdenes.DataBind();
            }
        }

        protected void lblOrdenes_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<OrdenProducto> ordenes = OrdenBRL.OrdenPedi(int.Parse(lblOrdenes.SelectedValue));
            foreach (OrdenProducto item in ordenes)
            {
                lblProOrd.Items.Add(ProductoBRL.Get(item.IdProducto).NombreProducto + " " + item.Cantidad);
            }
        }

        protected void btnNuevo_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmNuevaOrden.aspx");
        }
        protected void btnAtender_Click(object sender, EventArgs e)
        {
            Response.Redirect("frmRecibo.aspx");

        }
        protected void btnEliminar_Click(object sender, EventArgs e)
        {

            OrdenBRL.Delete(short.Parse(lblOrdenes.SelectedValue));
            lblOrdenes.DataSource = OrdenBRL.Select();
            lblOrdenes.DataValueField = "idOrden";
            lblOrdenes.DataTextField = "idMesa";
            lblOrdenes.DataBind();

			Response.Write("<script language=javascript>alert('Eliminado con éxito." + "')</script>");
		}
        
    }
}