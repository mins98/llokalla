﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;

namespace Visual
{
	public partial class Index : System.Web.UI.MasterPage
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (Sesion.IdUsuario != 0)
			{
				switch (Sesion.Role)
				{
					case 1:
						liConfiguracion.Visible = false;
						liReserva.Visible = false;
						break;
					case 2:
						liOrden.Visible = false;
						liRecibo.Visible = false;
						liConfiguracion.Visible = false;
						break;
				}
			}
			else
				Response.Redirect("frmLogIn.aspx");
		}
	}
}