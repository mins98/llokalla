﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmLogIn.aspx.cs" Inherits="Visual.frmLogIn" %>

<!DOCTYPE html lang="es">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>

    <title>Formulario Login y Registro de Usuarios</title>
	 <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway|Ubuntu" rel="stylesheet"/>

    <!-- Estilos -->
	<link rel="stylesheet" href="CSS/login.css"/>
</head>
	
<body>
	<!-- Formulario -->
	<div class="contenedor-formularios">
		<!-- Links de los formularios -->
		<ul class="contenedor-tabs">
			<li class="tab tab-segunda active"><a href="#iniciar-sesion" runat="server">Iniciar Sesión</a></li>
			<li class="tab tab-primera"><a href="frmRegistrar.aspx" runat="server">Registrarse</a></li>
		</ul>
		<!-- Contenido de los Formularios -->
		<div class="contenido-tab">
			<!-- Iniciar Sesion -->
			<div id="iniciar-sesion"> 
				<h1>Iniciar Sesión</h1>
				<form runat="server" id="frmLogIn">
					<div class="contenedor-input">
						<label>
							Nombre de Usuario
						</label>
						<asp:TextBox ID="txtUserNameL" runat="server"/>
					</div>
					<!--Contraseña-->
					<div class="contenedor-input">
						<label>
							Contraseña
						</label>
						<asp:TextBox id="txtPasswordL" runat="server"/>
					</div>
					<asp:Button class="button button-block" Text="Iniciar" runat="server" onClick="btnLogin_Click"/>
				</form>
			</div>
		</div>
	</div>

	<script src="JS/jquery.js"></script>
	<script src="JS/main.js"></script>
</body>
</html>
