﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;


namespace Visual
{
	public partial class frmRegistrar : System.Web.UI.Page
	{

		protected void btnRegistrarse_Click(object sender, EventArgs e)
		{
            if (true)
            {
                if (txtPasswordR.Text == txtPasswordRTwo.Text)
                {
                    try
                    {
                        List<Telefono> listaTelefono = new List<Telefono>();

                        for (int i = 0; i < lbxLista.Items.Count; i++)
                        {

                            listaTelefono.Add(new Telefono(lbxLista.Items[i].Value));
                        }
                        UsuarioBRL.Insert(new Persona(txtName.Text, txtFirstLastName.Text, txtSecondLastName.Text, cldDate.SelectedDate, txtDireccion.Text, txtCi.Text, byte.Parse(lbxGenero.SelectedValue)), new Usuario(txtuserNameR.Text, txtPasswordR.Text, 0), listaTelefono);

                        DataTable result = UsuarioBRL.Login(txtuserNameR.Text, txtPasswordR.Text);

                        Response.Redirect("frmLogIn.aspx");
                        Response.Write("<script language=javascript>alert('Insertado con éxito." + "')</script>");
                    }
                    catch (Exception ex)
                    {

                        Response.Write("<script language=javascript>alert('Se genero un error comuniquese con el administrador de sistemas" + ex + "." + "')</script>");
                    }
                }
                else
                {
                    Response.Write("<script language=javascript>alert('Las contraseñas no coinciden." + "')</script>");
                    Response.Redirect("frmRegistrar.aspx");
                }

            }


		}
		protected void btnTelefono_Click(object sender, EventArgs e)
		{
			if (txtTelefono.Text != "")
				lbxLista.Items.Add(txtTelefono.Text);
		}

		protected void lbxLista_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
	}
}