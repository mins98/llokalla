﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmRegistrar.aspx.cs" Inherits="Visual.frmRegistrar" %>
<%@ Register TagPrefix="recaptcha" Namespace="Recaptcha" Assembly="Recaptcha" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"/>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <title>Formulario Login y Registro de Usuarios</title>
	 <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway|Ubuntu" rel="stylesheet"/>

    <!-- Estilos -->
    <link rel="stylesheet" href="CSS/refistrar.css"/>
</head>
<body>
		<!-- Formulario -->
	<div class="contenedor-formularios">
		<!-- Links de los formularios -->
		<ul class="contenedor-tabs">
			<li class="tab tab-segunda active"><a href="frmLogIn.aspx" runat="server">Iniciar Sesión</a></li>
			<li class="tab tab-primera"><a href="#registrarse" runat="server">Registrarse</a></li>
		</ul>
		<!-- Contenido de los Formularios -->
		<div class="contenido-tab">
		<!-- Registrarse -->
			<div id="registrarse">
				<h1>Registrarse</h1>
				<form runat="server" id="frmRegistrarse" name="frmRegistrarse">
					<!--Nombre y CI-->
					<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								C.I.
							</label>
							<br />
							<asp:TextBox id="txtCi" runat="server"/>
						</div>
						<div class="contenedor-input">
							<label>
								Nombre
							</label>
							<br />
							<asp:TextBox runat="server" id="txtName"/>
						</div>
					</div>
					<!--Apellidos-->
					<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Primer Apellido
							</label>
							<br />
							<asp:TextBox runat="server" id="txtFirstLastName" />
						</div>
						<div class="contenedor-input">
							<label>
								Segundo Apellido
							</label>
							<br />
							<asp:TextBox runat="server" id="txtSecondLastName" />
						</div>
					</div>
					<!--Nombre Usuario-->
					<div class="contenedor-input">
						<label>
							Nombre de Usuario
						</label>
						<br />
						<<asp:TextBox runat="server" ID="txtuserNameR"/>
					</div>
					<!--Telefono-->
					<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Telefono
							</label>
							<br />
							<asp:TextBox runat="server" id="txtTelefono"/>
						</div>
						<div class="contenedor-input">
							<asp:Button class="button button-not-block" text="Insertar" runat="server" onClick="btnTelefono_Click"/>
						</div>
						<div class="contenedor-input">	
							<asp:ListBox id="lbxLista" runat="server" Rows="10" SelectionMode="Single">

							</asp:ListBox>
						</div>
					</div>
					<!--Contraseña 1-->
					<div class="contenedor-input">
						<label>
							Contraseña
						</label>
						<br />
						<asp:TextBox runat="server" ID="txtPasswordR"/>
					</div>
					<!--Contraseña 2-->
					<div class="contenedor-input">
						<label>
							Repetir Contraseña  
						</label>
						<br />
						<asp:TextBox runat="server" id="txtPasswordRTwo"/>
					</div>
					<!--Género-->
					<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Género
							</label>
						</div>
						<div class="contenedor-input">
							<asp:ListBox id="lbxGenero" runat="server" Rows="3" SelectionMode="Single">
								<asp:ListItem Value="0">Mujer</asp:ListItem>
								<asp:ListItem Value="1">Hombre</asp:ListItem>
								<asp:ListItem Value="2">Indefinido</asp:ListItem>
							</asp:ListBox>
						</div>
					</div>
					<!--Fecha Nacimineto-->
					<div class="fila-arriba">
						<div class="contenedor-input">
							<label>
								Fecha Nacimiento  
							</label>
						</div>
						<div class="contenedor-input">
							<asp:Calendar runat="server" ID="cldDate"/>
						</div>
					</div>		
					<!--Dirección-->
					<div class="contenedor-input">
						<label>
							Dirección 
						</label>
						<br />
						<asp:TextBox runat="server" ID="txtDireccion"/>
					</div>
                 <div class="g-recaptcha" data-sitekey="6LdJBHUUAAAAACI-EFGQY69TtVCyZiDyL8T95iU6"></div>
					<asp:Button class="button button-block" Text="Registrarse" runat="server" onClick="btnRegistrarse_Click"/>
				</form>
			</div>
		</div>
	</div>

	<script src="JS/jquery.js"></script>
	<script src="JS/main.js"></script>
</body>
</html>
