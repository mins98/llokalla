﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using BRL;

namespace Visual
{
	public partial class frmReservasCRUD : System.Web.UI.Page
	{
		Usuario usuario = null;
		Mesa mesa = null;
		protected void Page_Load(object sender, EventArgs e)
		{
			cargar();
		}
		public void cargar()
		{
			if (!Page.IsPostBack)
			{
				lblReservas.DataSource = ReservaBRL.Select();
				lblReservas.DataValueField = "idReserva";
				lblReservas.DataTextField = "fechaReserva";
				lblReservas.DataBind();
			}
		}

		protected void btnInsertar_Click(object sender, EventArgs e)
		{
			try
			{
				usuario = UsuarioBRL.getUsuario(UsuarioBRL.getIdPersona(txtCI.Text));
				mesa = MesaBRL.GetTableByNumber(txtNroMesa.Text);
				ReservaBRL.Insert(new Reserva(DateTime.Parse(txtFechaReserva.Text), DateTime.Parse(txtFechaExpiración.Text), usuario.IdUsuario, mesa.IdMesa));
				limpiador();
				Response.Write("<script language=javascript>alert('Se insertó con éxito.')</script>");
			}
			catch (Exception ex)
			{
				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}
		public void limpiador()
		{
			txtCI.Text = "";
			txtCliente.Text = "";
			txtFechaExpiración.Text = "";
			txtFechaReserva.Text = "";
			txtNroMesa.Text = "";
			lblReservas.DataSource = ReservaBRL.Select();
			lblReservas.DataValueField = "idReserva";
			lblReservas.DataTextField = "fechaReserva";
			lblReservas.DataBind();
		}
		public void mostrar()
		{
			Reserva reserva = ReservaBRL.Get(short.Parse(lblReservas.SelectedValue));
			mesa = MesaBRL.Get(reserva.IdMesa);
			usuario = UsuarioBRL.getUsuario(reserva.IdUsuario);
			Persona persona = UsuarioBRL.getPersona(usuario.IdPersona);

			txtCI.Text = persona.Ci;
			txtCliente.Text = persona.Nombre;
			txtFechaExpiración.Text = reserva.FechaExpiracion.ToString();
			txtFechaReserva.Text = reserva.FechaReserva.ToString();
			txtNroMesa.Text = mesa.NroMesa.ToString();
		}
		protected void lblReservas_SelectedIndexChanged(object sender, EventArgs e)
		{
			mostrar();
		}

		protected void btnModificar_Click(object sender, EventArgs e)
		{
			Reserva reserva = ReservaBRL.Get(short.Parse(lblReservas.SelectedValue));
			reserva.FechaExpiracion= DateTime.Parse(txtFechaExpiración.Text);
			reserva.FechaReserva = DateTime.Parse(txtFechaReserva.Text);

			ReservaBRL.Update(reserva);
			limpiador();
			mostrar();
		}

		protected void btnEliminar_Click(object sender, EventArgs e)
		{
			ReservaBRL.Delete(ReservaBRL.Get(short.Parse(lblReservas.SelectedValue)));
			limpiador();
		}
	}
}