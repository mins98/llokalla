﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;

namespace Visual
{
	public partial class frmLogout : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnYes_Click(object sender, EventArgs e)
		{
			Sesion.IdUsuario = 0;
			Response.Redirect("frmLogIn.aspx");
		}

		protected void btnNo_Click(object sender, EventArgs e)
		{
			switch (Sesion.Role)
			{
				case 0:
				case 1:
					Response.Redirect("frmOrden.aspx");
					break;
				case 2:
					Response.Redirect("frmReserva.aspx");
					break;
			}
		}
	}
}