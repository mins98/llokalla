﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using BRL;

namespace Visual
{
	public partial class frmMesas : System.Web.UI.Page
	{
		Mesa mesa = null;
		protected void Page_Load(object sender, EventArgs e)
		{
			cargar();
		}
		public void cargar()
		{
			if (!Page.IsPostBack)
			{
				lblMesas.DataSource = MesaBRL.Select();
				lblMesas.DataValueField = "idMesa";
				lblMesas.DataTextField = "nroMesa";
				lblMesas.DataBind();
			}
		}

		protected void btnInsertar_Click(object sender, EventArgs e)
		{
			try
			{
				if (!MesaBRL.GetByNumber(txtNroMesa.Text))
				{
					
						mesa = new Mesa(short.Parse(txtNroMesa.Text), txtUbicacion.Text, byte.Parse(txtMaximo.Text));

						MesaBRL.Insert(mesa);

						limpiador();
						Response.Write("<script language=javascript>alert('Se insertó con éxito.')</script>");
				}
				else
					Response.Write("<script language=javascript>alert('Ya existe la Persona.')</script>");
			}
			catch (Exception ex)
			{
				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}
		protected void btnModificar_Click(object sender, EventArgs e)
		{

			try
			{
				mesa = MesaBRL.Get(byte.Parse(lblMesas.SelectedValue));

				mesa.CantidadMaxima = byte.Parse(txtMaximo.Text);
				mesa.NroMesa = short.Parse(txtNroMesa.Text);
				mesa.Ubicacion = txtUbicacion.Text;

				MesaBRL.Update(mesa);
			
				Response.Write("<script language=javascript>alert('Se Modificó con éxito.')</script>");

				limpiador();

			}
			catch (Exception ex)
			{

				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}
		protected void btnElimiar_Click(object sender, EventArgs e)
		{
			try
			{
				mesa = MesaBRL.Get(byte.Parse(lblMesas.SelectedValue));

				MesaBRL.Delete(mesa.IdMesa);

				Response.Write("<script language=javascript>alert('Se borró con éxito.')</script>");

				limpiador();

			}
			catch (Exception ex)
			{
				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}
		protected void lblMesas_SelectedIndexChanged(object sender, EventArgs e)
		{
			Mostrar();
		}
		public void Mostrar()
		{
			mesa = MesaBRL.Get(byte.Parse(lblMesas.SelectedValue));

			txtUbicacion.Text = mesa.Ubicacion;
			txtNroMesa.Text = mesa.NroMesa.ToString();
			txtMaximo.Text = mesa.CantidadMaxima.ToString();
		}

		public void limpiador()
		{
			txtMaximo.Text = "";
			txtNroMesa.Text = "";
			txtUbicacion.Text = "";
			cargar();
		}
	}
}