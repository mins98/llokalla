﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Index.Master" AutoEventWireup="true" CodeBehind="frmReservasCRUD.aspx.cs" Inherits="Visual.frmReservasCRUD" %>
<asp:Content ID="Content1" ContentPlaceHolderID="title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="body" runat="server">
	  <!-- Formulario -->
	<div class="contenedor-formulario">
		<!-- Reservas -->
		<div id="Reservas">
            <div  class="fila-arriba">
                <div class="contenedor-input">
                <label>
				Reservas
			    </label>
                    <br />
                    <br />
                    <br />
                <asp:ListBox ID="lblReservas" runat="server" OnSelectedIndexChanged="lblReservas_SelectedIndexChanged" Width="709px" AutoPostBack="True"></asp:ListBox>
            </div>
            </div>
			<div class="fila-arriba">
				<div class="contenedor-input">
							<label>
								Fecha Expiración 
							</label>
							<br/>
							<asp:TextBox id="txtFechaExpiración" runat="server"/>
						</div>
				<div class="contenedor-input">
					<label>
						Fecha de Reserva
					</label>
						<br/>
					<asp:TextBox runat="server" id="txtFechaReserva"/>
				</div>	
			</div>
			<div class="contenedor-input">
				<label>
					Número de Mesa
				</label>
					<br/>
				<asp:TextBox runat="server" id="txtNroMesa"/>
			</div>	
			<div class="contenedor-input">
				<label>
					Nombre Cliente
				</label>
					<br/>
				<asp:TextBox runat="server" id="txtCliente"/>
			</div>	
			<div class="contenedor-input">
				<label>
					CI
				</label>
					<br/>
				<asp:TextBox runat="server" id="txtCI"/>
			</div>	
		</div>	
        <div>
			<div class="contenedor-input">
				<asp:Button  class="button button-block" Text="Insertar" runat="server" onClick="btnInsertar_Click" UseSubmitBehavior="False"  />
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Modificar" runat="server" OnClick="btnModificar_Click" UseSubmitBehavior="False" />
			</div>
			<div class="contenedor-input">
				<asp:Button class="button button-block" Text="Eliminar" runat="server" OnClick="btnEliminar_Click" UseSubmitBehavior="False" />
			</div>
		</div>
	</div> 
</asp:Content>
