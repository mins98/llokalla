﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using BRL;
using System.Data;

namespace Visual
{
	public partial class frmLogIn : System.Web.UI.Page
	{
		protected void btnLogin_Click(object sender, EventArgs e)
		{
			int count = 0;

			if (txtUserNameL.Text.Trim() != "" && txtPasswordL.Text.Trim() != "")
			{
				try
				{
					DataTable result =  UsuarioBRL.Login(txtUserNameL.Text, txtPasswordL.Text);

					if (result.Rows.Count > 0)
					{
						Sesion.Active = result.Rows[0][2].ToString();
						Sesion.Adress = result.Rows[0][10].ToString();
						Sesion.BirthDay = DateTime.Parse(result.Rows[0][9].ToString());
						Sesion.CI = result.Rows[0][5].ToString();
						Sesion.Gender = byte.Parse(result.Rows[0][11].ToString());
						Sesion.IdPersona = int.Parse(result.Rows[0][4].ToString());
						Sesion.IdUsuario = int.Parse(result.Rows[0][0].ToString());
						Sesion.MLastName = result.Rows[0][8].ToString();
						Sesion.Name = result.Rows[0][6].ToString();
						Sesion.PLastName = result.Rows[0][7].ToString();
						Sesion.Role = byte.Parse(result.Rows[0][3].ToString());
						Sesion.UserName = result.Rows[0][1].ToString();

						switch (Sesion.Role)
						{
							case 0:
							case 1:
								Response.Redirect("frmOrden.aspx");
								break;
							case 2:
								Response.Redirect("frmReserva.aspx");
								break;
						}
					}
					else
					{
						count++;
						if (count > 3)
						{
							ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
						}
						Response.Write("<script language=javascript>alert('No se encuentra una cuenta o contraseña parecida, le quedan " + count + " intentos." + "')</script>");
					}
				}
				catch (Exception ex)
				{
					Response.Write("<script language=javascript>alert('Se genero un error comuniquese con el administrador de sistemas" + ex + "." + "')</script>");
				}
			}
			else
			{
				Response.Write("<script language=javascript>alert('Ingresó incorrectamente los datos.')</script>");
			}
		}		

	}
}