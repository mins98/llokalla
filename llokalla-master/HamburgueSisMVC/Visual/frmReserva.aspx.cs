﻿using System;
using System.Web.UI;
using Common;
using BRL;

namespace Visual
{
	public partial class frmReserva : System.Web.UI.Page
	{
		Mesa mesa = null;
		protected void Page_Load(object sender, EventArgs e)
		{
			cargar();
		}
		public void cargar()
		{
			if (!Page.IsPostBack)
			{
				lblMesas.DataSource = MesaBRL.Select();
				lblMesas.DataValueField = "idMesa";
				lblMesas.DataTextField = "nroMesa";
				lblMesas.DataBind();
			}
		}
		protected void lblMesas_SelectedIndexChanged(object sender, EventArgs e)
		{
			Mostrar();
		}
		public void Mostrar()
		{
			mesa = MesaBRL.Get(byte.Parse(lblMesas.SelectedValue));

			txtUbicacion.Text = mesa.Ubicacion;
			txtNroMesa.Text = mesa.NroMesa.ToString();
			txtMaximo.Text = mesa.CantidadMaxima.ToString();
		}

		protected void btnReservar_Click(object sender, EventArgs e)
		{
			try
			{
				ReservaBRL.Insert(new Reserva(DateTime.Now, calendarioExpiración.SelectedDate.Date, Sesion.IdUsuario,byte.Parse( lblMesas.SelectedValue)));


				Response.Write("<script language=javascript>alert('" + Sesion.Name + ",se realizó la reserva con éxito.')</script>");
			}
			catch (Exception ex)
			{
				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}
	}
}