﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using BRL;

namespace Visual
{
    public partial class frmProductos : System.Web.UI.Page
    {
        Producto pr;
        protected void Page_Load(object sender, EventArgs e)
        {
            cargar();
        }
        public void cargar()
        {
            if (!Page.IsPostBack)
            {
                lblProductos.DataSource = ProductoBRL.Select();
                lblProductos.DataValueField = "idProducto";
                lblProductos.DataTextField = "nombreProducto";
                lblProductos.DataBind();
            }
        }

        protected void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                ProductoBRL.Insert(new Producto(txtNombre.Text,double.Parse(txtPrecio.Text), byte.Parse(lbxTipo.SelectedValue), txtDescripcion.Text));
                limpiador();
                Response.Write("<script language=javascript>alert('Se insertó con éxito.')</script>");
            }
            catch (Exception ex)
            {
                Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
            }
        }
        public void limpiador()
        {
            txtNombre.Text = "";
            txtPrecio.Text = "";
            txtDescripcion.Text = "";
            lblProductos.DataSource = ProductoBRL.Select();
            lblProductos.DataValueField = "idProducto";
            lblProductos.DataTextField = "nombreProducto";
            lblProductos.DataBind();
        }
        public void mostrar()
        {
            pr = ProductoBRL.Get(short.Parse(lblProductos.SelectedValue));
            txtNombre.Text = pr.NombreProducto;
            txtPrecio.Text = pr.Precio.ToString();
            txtDescripcion.Text = pr.Descripcion;
            lbxTipo.SelectedValue = pr.Tipo.ToString();
        }
        protected void lblProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            mostrar();
        }

        protected void btnModificar_Click(object sender, EventArgs e)
        {
            pr = ProductoBRL.Get(short.Parse(lblProductos.SelectedValue));
            pr.NombreProducto = txtNombre.Text;
            pr.Descripcion =txtDescripcion.Text;
            pr.Precio = double.Parse(txtPrecio.Text);
            pr.Tipo = byte.Parse(lbxTipo.SelectedValue);
            ProductoBRL.Update(pr);
            limpiador();
        }
        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            
            ProductoBRL.Delete(short.Parse(lblProductos.SelectedValue));
            limpiador();
        }
    }
}