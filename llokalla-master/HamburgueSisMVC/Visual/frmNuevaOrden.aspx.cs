﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using BRL;
using Common;

namespace Visual
{
    public partial class frmNuevaOrden : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargar();
        }

        public void cargar()
        {
            if (!Page.IsPostBack)
            {
                lblMesas.DataSource = MesaBRL.Select();
                lblMesas.DataValueField = "idMesa";
                lblMesas.DataTextField = "ubicacion";
                lblMesas.DataBind();
                lblProducto.DataSource = ProductoBRL.Select();
                lblProducto.DataValueField = "idProducto";
                lblProducto.DataTextField = "nombreProducto";
                lblProducto.DataBind();
            }
        }

        protected void btnOrdnar_Click(object sender, EventArgs e)
        {
            List<OrdenProducto> lista = new List<OrdenProducto>();

            for (int i = 0; i < lblagregados.Items.Count; i++)
            {
                lista.Add(new OrdenProducto(short.Parse(lblId.Items[i].Text),byte.Parse(lblCantidad.Items[i].Text),ProductoBRL.Get(short.Parse(lblId.Items[i].Text)).Precio));
            }

            Orden or = new Orden(1, byte.Parse(lblMesas.SelectedValue));
            OrdenBRL.Insert(or, lista);


			Response.Write("<script language=javascript>alert('Se creó la orden." + "')</script>");

			Response.Redirect("frmOrden.aspx");
		}

        protected void btnIngres_Click(object sender, EventArgs e)
        {
            lblagregados.Items.Add(lblProducto.SelectedItem.Text);
            lblCantidad.Items.Add(txtCant.Text);
            lblId.Items.Add(lblProducto.SelectedValue);

		}
    }
}