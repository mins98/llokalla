﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Common;
using BRL;
using System.Data;

namespace Visual
{
	public partial class frmUsuarios : System.Web.UI.Page
	{
		Persona persona = null;
		Usuario usuario = null;
		List<Telefono> listaTelefono = null;

		protected void btnTelefono_Click(object sender, EventArgs e)
		{
			if (txtTelefono.Text != "")
				lbxLista.Items.Add(txtTelefono.Text);
		}
		protected void Page_Load(object sender, EventArgs e)
		{
            cargar();
		}
        public void cargar()
        {
            if (!Page.IsPostBack)
            {
                lbUsuarios.DataSource = UsuarioBRL.listaUs();
                lbUsuarios.DataValueField = "idUsuario";
                lbUsuarios.DataTextField = "userName";
                lbUsuarios.DataBind();
            }
        }
		protected void btnInsertar_Click(object sender, EventArgs e)
		{
			try
			{
				if (! UsuarioBRL.getPersonByCi(txtCi.Text))
				{
					if (txtPasswordR.Text == txtPasswordRTwo.Text)
					{
						 persona = new Persona(txtName.Text, txtFirstLastName.Text, txtSecondLastName.Text, cldDate.SelectedDate, txtDireccion.Text, txtCi.Text, byte.Parse(lbxGenero.SelectedValue));
						 usuario = new Usuario(txtuserNameR.Text, txtPasswordR.Text, byte.Parse(lbxTipo.SelectedValue));
						listaTelefono = new List<Telefono>();

						for (int i = 0; i < lbxLista.Items.Count; i++)
						{

							listaTelefono.Add(new Telefono(lbxLista.Items[i].Value));
						}


                        UsuarioBRL.Insert(persona, usuario, listaTelefono);
                        listaTelefono = new List<Telefono>();
                       
                        limpiador();
                        Response.Write("<script language=javascript>alert('Se insertó con éxito.')</script>");
					}
					else
						Response.Write("<script language=javascript>alert('Repita las contraseñas.')</script>");
				}
				else
					Response.Write("<script language=javascript>alert('Ya existe la Persona.')</script>");
			}
			catch (Exception ex)
			{
				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}

		protected void btnModificar_Click(object sender, EventArgs e)
		{
			try
			{
                if (txtPasswordR.Text == txtPasswordRTwo.Text)
                {
                    persona = UsuarioBRL.getPersona(int.Parse(lbUsuarios.SelectedValue));
                    usuario = UsuarioBRL.getUsuario(int.Parse(lbUsuarios.SelectedValue));
                    persona.Ci = txtCi.Text;
                    persona.Direccion = txtDireccion.Text;
                    persona.PrimerAp = txtFirstLastName.Text;
                    persona.Nombre = txtName.Text;
                    persona.SegundoAp = txtSecondLastName.Text;
                    usuario.Tipo = byte.Parse(lbxTipo.SelectedValue);
                    usuario.UserName = txtuserNameR.Text;
                    persona.FechaNac = cldDate.SelectedDate;

                    usuario.Password = txtPasswordR.Text;
                    persona.Genero = byte.Parse(lbxGenero.SelectedValue);
                    listaTelefono = new List<Telefono>();

                    for (int i = 0; i < lbxLista.Items.Count; i++)
                    {

                        listaTelefono.Add(new Telefono(lbxLista.Items[i].Value));
                    }

                    UsuarioBRL.Update(persona, usuario, listaTelefono);

                    Response.Write("<script language=javascript>alert('Se Modificó con éxito.')</script>");
                   
                    limpiador();
                    listaTelefono = new List<Telefono>();
                }
            }
			catch (Exception ex)
			{

				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}

		protected void btnElimiar_Click(object sender, EventArgs e)
		{
			try
			{
             
                persona = UsuarioBRL.getPersona(int.Parse(lbUsuarios.SelectedValue));
                usuario = UsuarioBRL.getUsuario(int.Parse(lbUsuarios.SelectedValue));


				
                listaTelefono = TelefonoBRL.listaTelefonos(usuario.IdUsuario);
                foreach (var item in listaTelefono)
				{
                    TelefonoBRL.eliminarTelefono(item.IdTelefono);
				}

                UsuarioBRL.Delete(usuario.IdUsuario);

				Response.Write("<script language=javascript>alert('Se borró con éxito.')</script>");

               
               
                limpiador();
                listaTelefono = new List<Telefono>();
				
			}
			catch (Exception ex)
			{
				Response.Write("<script language=javascript>alert('" + ex.Message + "')</script>");
			}
		}
		public void Mostrar()
		{
            persona = UsuarioBRL.getPersona(int.Parse(lbUsuarios.SelectedValue));
            usuario = UsuarioBRL.getUsuario(int.Parse(lbUsuarios.SelectedValue));

			listaTelefono  = TelefonoBRL.listaTelefonos(usuario.IdUsuario);
            lbxLista2.DataSource = listaTelefono;
            lbxLista2.DataValueField = "idTelefono";
            lbxLista2.DataTextField = "numero";
            lbxLista2.DataBind();
            lbxLista.Items.Clear();
            txtCi.Text = persona.Ci;
			txtDireccion.Text = persona.Direccion;
			txtFirstLastName.Text = persona.PrimerAp;
			txtName.Text = persona.Nombre;
			txtSecondLastName.Text = persona.SegundoAp;
			lbxTipo.Text = usuario.Tipo.ToString();
			txtuserNameR.Text = usuario.UserName;
			cldDate.SelectedDate = persona.FechaNac;
			lbxGenero.SelectedValue = persona.Genero.ToString();
		}

        protected void lbUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostrar();
        }
        public void limpiador()
        {
            txtName.Text = string.Empty;
            txtFirstLastName.Text = "";
            txtSecondLastName.Text = "";
            txtDireccion.Text = "";
            txtCi.Text = "";
            txtDireccion.Text = persona.Direccion;
            txtuserNameR.Text = "";
            txtPasswordR.Text = "";
            txtPasswordRTwo.Text = "";
            lbxLista.Items.Clear();
            lbxLista2.Items.Clear();
            lbUsuarios.DataSource = UsuarioBRL.listaUs();
            lbUsuarios.DataValueField = "idUsuario";
            lbUsuarios.DataTextField = "userName";
            lbUsuarios.DataBind();
        }
    }
}