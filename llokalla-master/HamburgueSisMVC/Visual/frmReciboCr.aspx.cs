﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;

namespace Visual
{
    public partial class frmReciboCr : System.Web.UI.Page
    {
        int id;
        protected void Page_Load(object sender, EventArgs e)
        {
           
            id =int.Parse(Request.QueryString["id"]);
            ReportDocument rtp = new ReportDocument();
            rtp.Load(Server.MapPath("~/CRRecibo.rpt"));
            rtp.SetParameterValue("@id", id);
            Cr.ReportSource = rtp;

        }
    }
}