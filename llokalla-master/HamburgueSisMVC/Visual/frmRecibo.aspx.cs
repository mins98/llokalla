﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BRL;
using Common;

namespace Visual
{
    public partial class frmRecibo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            cargar();
        }
        public void cargar()
        {
            if (!Page.IsPostBack)
            {
                lblOrdenes.DataSource = OrdenBRL.Select();
                lblOrdenes.DataValueField = "idOrden";
                lblOrdenes.DataTextField = "idOrden";
                lblOrdenes.DataBind();
            }
        }

        protected void btnInsetar_Click(object sender, EventArgs e)
        {
            lbPedi.Items.Add(lblOrdenes.Items[lblOrdenes.SelectedIndex].Value);
			MesaBRL.Desocupar(OrdenBRL.Get(lblOrdenes.SelectedValue).IdMesa);

			Response.Write("<script language=javascript>alert('Se Insertó con éxito." + "')</script>");
		}

        protected void btnRecibo_Click(object sender, EventArgs e)
        {
            double total = 0;
            List<int> orde = new List<int>();
            Recibo rec = new Recibo(txtNit.Text, txtName.Text);

            for (int i = 0; i < lbPedi.Items.Count; i++)
            {
                orde.Add(int.Parse(lbPedi.Items[i].Text));

				foreach (OrdenProducto item in OrdenBRL.OrdenPedi(int.Parse(lbPedi.Items[i].Value)))
                {
					total += item.Precio * item.Cantidad;
                }
            }
            rec.Total = total;
			
            ReciboBRL.Insert(rec, orde);
            Response.Redirect("frmReciboCr.aspx?id=" + ReciboBRL.ultimoId());
        }
    }
}