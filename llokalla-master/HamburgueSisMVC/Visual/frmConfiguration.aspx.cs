﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Visual
{
	public partial class frmConfiguration : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnPerdiles_Click(object sender, EventArgs e)
		{

			Response.Redirect("frmUsuarios.aspx");
		}

		protected void btnMesas_Click(object sender, EventArgs e)
		{

			Response.Redirect("frmMesas.aspx");
		}

		protected void btnProductos_Click(object sender, EventArgs e)
		{

			Response.Redirect("frmProductos.aspx");
		}

		protected void btnReservas_Click(object sender, EventArgs e)
		{

			Response.Redirect("frmReservasCRUD.aspx");
		}
	}
}